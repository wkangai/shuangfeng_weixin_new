package com.shuangfeng.weixin.service.condition.core;

import org.hibernate.Query;


/**
 * Using to keep the query conditions.
 * @author 
 *
 */
public interface Condition {

	public final static String A = "A";
	public final static String B = "B";
	public final static String C = "C";
	
	String toCondition();
	String getOrderBy();
	String getSql();
	
	int getMaxResult();
	int getPageIndex();
	void setPageIndex(int pageIndex);
	int getCountInPage();
	
	void postHandle(Query query);
}
