package com.shuangfeng.weixin.service.condition.core;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.type.Type;

import com.shuangfeng.weixin.common.dist.Const;
import com.shuangfeng.weixin.common.util.DateUtil;


/**
 * Using to keep the query conditions.
 * Not a persistence object.
 * @author 
 *
 */
public abstract class BaseQueryCondition implements Condition{
	
	private String beginDate;
	private String endDate;
	private int datePoint = Const.DATEPOINT_NONE;
	private Long loginInfoId;
	private int pageIndex = -1;
	private String sql;
	private int maxResult = -1;
	private String orderBy = null;
	private boolean beginEndDateNoHourMinSec = false;
	private int countInPage = Const.PARAM_NO_INT;
	private String beginEndDateField;
	private HashMap<String, Object> postHandleMeta = new HashMap<String, Object>();
	private HashMap<String, Type> postHandleMetaType = new HashMap<String, Type>();
	private String defaultBaseTableAlian;
	
	protected void init(){}
	
	public int getCountInPage(){
		if(this.countInPage == Const.PARAM_NO_INT){
			this.countInPage = 15;
		}
		return this.countInPage;
	}
	
	public void setCountInPage(int countInPage) {
		this.countInPage = countInPage;
	}
	
	public boolean isBeginEndDateNoHourMinSec() {
		return beginEndDateNoHourMinSec;
	}
	
	public void setBeginEndDateNoHourMinSec(boolean beginEndDateNoHourMinSec) {
		this.beginEndDateNoHourMinSec = beginEndDateNoHourMinSec;
	}
	
	public String getSql() {
		return this.sql;
	}
	
	public void setSql(String sql) {
		this.sql = sql;
	}
	
	public int getMaxResult() {
		return this.maxResult;
	}
	
	public void setMaxResult(int maxResult) {
		this.maxResult = maxResult;
	}
	
	public String getOrderBy() {
		return this.orderBy;
	}
	
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	protected String getOwnerIdName(){
		return "loginInfoId";
	}
	
	protected int getState(){
		return -1;
	}
	
	protected String getStateName(){
		return null;
	}
	
	public String getDefaultBaseTableAlian() {
		if(!StringUtils.isEmpty(this.defaultBaseTableAlian)){
			return this.defaultBaseTableAlian;
		}
		return Condition.A;
	}
	
	public void setDefaultBaseTableAlian(String defaultBaseTableAlian) {
		this.defaultBaseTableAlian = defaultBaseTableAlian;
	}
	
	protected String getFieldWithBaseTableAlian(String field) {
		return this.getFieldWithBaseTableAlian(field, this.getDefaultBaseTableAlian());
	}
	
	protected String getFieldWithBaseTableAlian(String field, String tableAlian) {
		return tableAlian + "." + field;
	}
	
	public void setBeginEndDateField(String beginEndDateField) {
		this.beginEndDateField = beginEndDateField;
	}
	
	protected String getBeginEndDateField() {
		if(!StringUtils.isEmpty(this.beginEndDateField))
			return this.beginEndDateField;
		return "createdDate";
	}
	
	public String toCondition() {
		this.init();
		
		String condition = " 1=1 ";
		if(!StringUtils.isEmpty(this.getBeginDate()))
			condition += this.getDateConditionByBeginDate(this.getBeginDate());
		if(!StringUtils.isEmpty(this.getEndDate()))
			condition += this.getDateConditionByEndDate(this.getEndDate());
		
		String loginInfoCondition = this.getLoginInfoCondition();
		if(!StringUtils.isEmpty(loginInfoCondition))
			condition += loginInfoCondition;
		
		String customCondition = this.getCustomCondition();
		if(!StringUtils.isEmpty(customCondition))
			condition += customCondition;
		
		String datePointCondition = this.getConditionByDatePoint();
		if(!StringUtils.isEmpty(datePointCondition))
			condition += " and " + datePointCondition;
		
		return condition;
	}
	
	protected String getLoginInfoCondition() {
		if(this.getLoginInfoId() != null){
			return " and " + this.getFieldWithBaseTableAlian(this.getOwnerIdName()) + "=" + this.getLoginInfoId();
		}
		return "";
	}
	
	protected String getCustomCondition() {
		if(this.getState() != -1){
			String stateName = this.getStateName();
			return " and "+ this.getFieldWithBaseTableAlian(stateName) +"=" + this.getState();
		}
		return "";
	}
	
	private String getConditionByDatePoint() {
		Date now = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date startOfToday = calendar.getTime();
		
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		Date endOfToday = calendar.getTime();
		
		Date endDate = endOfToday;
		Date beginDate = startOfToday;
		switch (this.getDatePoint()) {
			default:
			case Const.DATEPOINT_NONE:
				return "";
			case Const.DATEPOINT_TODAY:
				calendar = null;
				break;
			case Const.DATEPOINT_THIS_WEEK:
				calendar.add(Calendar.WEEK_OF_YEAR, -1);
				break;
			case Const.DATEPOINT_THIS_MONTH:
				calendar.add(Calendar.MONTH, -1);
				break;
			case Const.DATEPOINT_THREE_MONTH:
				calendar.add(Calendar.MONTH, -3);
				break;
			case Const.DATEPOINT_HALF_YEAR:
				calendar.add(Calendar.MONTH, -6);
				break;
			case Const.DATEPOINT_ONEYEAR:
				calendar.add(Calendar.YEAR, -1);
				break;
			case Const.DATEPOINT_MORE_THAN_ONE_YEAR:
				calendar.add(Calendar.YEAR, -1);
				endDate = calendar.getTime();
				calendar = null;
				beginDate = null;
				break;
		}
		if(calendar != null)
			beginDate = calendar.getTime();
		
		String condition = " 1=1 ";
		if(beginDate != null)
			condition += this.getDateConditionByBeginDate(DateUtil.getDateYearMonthDay(beginDate));
		if(endDate != null)
			condition += this.getDateConditionByEndDate(DateUtil.getDateYearMonthDay(endDate));
		return condition;
	}
	
	/**
	 * date should always:****-**-**
	 */
	private String getDateConditionByBeginDate(String beginDate) {
		return this.getDateConditionByBeginDate(
				beginDate, 
				this.getBeginEndDateField(),
				this.getDefaultBaseTableAlian(), 
				this.isBeginEndDateNoHourMinSec());
	}
	
	protected String getDateConditionByBeginDate(String beginDate, String dateField, String tableAlian, boolean noHourMinSec) {
		String date = this.getFieldWithBaseTableAlian(dateField, tableAlian);
		if(noHourMinSec)
			return " and " + date + ">='" + beginDate + "' ";
		return " and " + date + ">='" + beginDate + " 00:00:00'";
	}
	
	/**
	 * date should always:****-**-**
	 */
	private String getDateConditionByEndDate(String endDate) {
		return this.getDateConditionByEndDate(
				endDate, 
				this.getBeginEndDateField(),
				this.getDefaultBaseTableAlian(), 
				this.isBeginEndDateNoHourMinSec());
	}
	
	protected String getDateConditionByEndDate(String endDate, String dateField, String tableAlian, boolean noHourMinSec) {
		String date = this.getFieldWithBaseTableAlian(dateField, tableAlian);
		if(noHourMinSec)
			return " and " + date + "<='" + endDate + "' ";
		return " and " + date + "<='" + endDate + " 23:59:59'";
	}
	
	public int getPageIndex() {
		return pageIndex;
	}
	
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	
	public String getBeginDate() {
		return beginDate;
	}
	
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}
	
	public String getEndDate() {
		return endDate;
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public Long getLoginInfoId() {
		return loginInfoId;
	}
	
	public void setLoginInfoId(Long loginInfoId) {
		this.loginInfoId = loginInfoId;
	}
	
	public int getDatePoint() {
		return datePoint;
	}
	
	public void setDatePoint(int datePoint) {
		this.datePoint = datePoint;
	}
	
	protected String addPostHandleMeta(String key, Object value, Type type){
		String holder = ":" + key;
		this.postHandleMeta.put(key, value);
		this.postHandleMetaType.put(key, type);
		return holder;
	}
	
	public void postHandle(Query query) {
		for (String key : this.postHandleMeta.keySet()) {
			query.setParameter(key, this.postHandleMeta.get(key), this.postHandleMetaType.get(key));
		}
	}
}
