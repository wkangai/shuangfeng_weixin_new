package com.shuangfeng.weixin.service.condition;

import com.shuangfeng.weixin.common.util.Util;
import com.shuangfeng.weixin.service.condition.core.BaseQueryCondition;

public class MenuQueryCondition extends BaseQueryCondition {

	private Long [] itemIds;
	
	@Override
	protected String getCustomCondition() {
		String condition = "";
		if(this.getItemIds()!=null && this.getItemIds().length>0){
			condition += String.format(" and m.id in (%1$s) ", Util.getInWhereCondition(getItemIds()));
		}
		return condition;
	}

	/**
	 * @return the itemIds
	 */
	public Long[] getItemIds() {
		return itemIds;
	}

	/**
	 * @param itemIds the itemIds to set
	 */
	public void setItemIds(Long[] itemIds) {
		this.itemIds = itemIds;
	}
	
}
