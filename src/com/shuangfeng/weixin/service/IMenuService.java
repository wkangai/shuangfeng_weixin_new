package com.shuangfeng.weixin.service;

import java.util.List;

import com.shuangfeng.weixin.entity.Menu;
import com.shuangfeng.weixin.service.condition.core.Condition;

public interface IMenuService {

	List<Menu> findByCondition(Condition condition);
	
	List<Menu> findAll();
	
	void add(Menu menu);
	
	void delete(Menu menu);
	
	void deletes(Long[] ids);
	
	void modify(Menu menu);
	
	Menu find(Long id);
	
}
