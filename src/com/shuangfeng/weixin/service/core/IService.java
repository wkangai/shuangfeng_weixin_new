package com.shuangfeng.weixin.service.core;

import com.shuangfeng.weixin.entity.core.BaseModel;


public interface IService<T extends BaseModel> {

	/**
	 * 
	 * @param t T
	 */
	void add(T t);
	/**
	 * 
	 * @param id
	 */
	void delete(T t);
	/**
	 * 
	 * @param t
	 */
	void update(T t);
	/**
	 * 
	 * @param id
	 * @return T
	 */
	T find(Long id);
	
}
