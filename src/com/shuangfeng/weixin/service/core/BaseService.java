package com.shuangfeng.weixin.service.core;

import java.util.Date;
import java.util.List;

import com.shuangfeng.weixin.dao.core.ConditionDAOImpl;
import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.service.condition.core.Condition;

public class BaseService <T extends BaseModel, E> implements IService<T>{

	protected ConditionDAOImpl<T> conditionDAOImpl;
	
	public List<E> getItems(Condition condition) {
		if(this.conditionDAOImpl != null)
			return this.conditionDAOImpl.getItems(condition);
		return null;
	}

	public int getItemsCount(Condition condition){
		if(this.conditionDAOImpl != null)
			return this.conditionDAOImpl.getItemsCount(condition);
		return 0;
	}

	/**
	 * @return the conditionDAOImpl
	 */
	public ConditionDAOImpl<T> getConditionDAOImpl() {
		return conditionDAOImpl;
	}

	/**
	 * @param conditionDAOImpl the conditionDAOImpl to set
	 */
	public void setConditionDAOImpl(ConditionDAOImpl<T> conditionDAOImpl) {
		this.conditionDAOImpl = conditionDAOImpl;
	}

	public void add(T t) {
		this.conditionDAOImpl.save(t);
	}

	public void delete(T t) {
		this.conditionDAOImpl.delete(t);
	}

	public void update(T t) {
		t.setModifiedDate(new Date());
		this.conditionDAOImpl.update(t);
	}

	public T find(Long id) {
		return this.conditionDAOImpl.getItem(id);
	}
	
	
}
