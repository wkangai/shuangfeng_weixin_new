package com.shuangfeng.weixin.service;

import java.util.List;

import com.shuangfeng.weixin.entity.Developer;


public interface IDeveloperService {

	List<Developer> findAll();
	
	void add(Developer menu);
	
	void delete(Developer menu);
	
	void modify(Developer menu);
	
	Developer find(Long id);
	
}
