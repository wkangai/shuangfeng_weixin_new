package com.shuangfeng.weixin.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import com.shuangfeng.weixin.dao.IUserDao;
import com.shuangfeng.weixin.dao.IUserGroupDao;
import com.shuangfeng.weixin.entity.User;
import com.shuangfeng.weixin.entity.UserGroup;
import com.shuangfeng.weixin.service.IUserGroupService;
import com.shuangfeng.weixin.service.condition.UserGroupQueryCondition;
import com.shuangfeng.weixin.service.core.BaseService;

public class UserGroupService extends BaseService<UserGroup, UserGroup> implements IUserGroupService{
	
	@Resource(name="userDao")
	private IUserDao userDao;
	
	@Override
	public List<UserGroup> findAll(){
		return super.getItems(new UserGroupQueryCondition());
	}
	
	@Override
	public void add(UserGroup group){
		super.add(group);
	}

	@Override
	public void synchronize(List<UserGroup> groups) {
		for(UserGroup group:groups){
			
		}
		throw new NotImplementedException();
		//Todo work
	}

	@Override
	public UserGroup find(Long id) {
		String userGroupId;
		UserGroup group;
		if(id==0){
			userGroupId="null";
			group=new UserGroup();
			group.setId(0L);
			group.setName("未分组");
			group.setGroupId(0);
		}
		else {
			userGroupId=id.toString();
			group=super.find(id);			
		}
		String baseQuery="from User as user where user.group is %s";
		String queryString=String.format(baseQuery, userGroupId);
		Set<User> users=userDao.getSet(queryString);
		group.setCount(users.size());
		group.setUsers(users);
		return group;
	}

	@Override
	public UserGroup find(int groupId) {
		IUserGroupDao userGroupDao=(IUserGroupDao)conditionDAOImpl;
		return userGroupDao.find(groupId);
	}
	
	public static void CloneGroup(UserGroup targetGroup,UserGroup sourceGroup){
		targetGroup.setName(sourceGroup.getName());
		targetGroup.setModifiedDate(new Date());
		targetGroup.setCount(sourceGroup.getCount());
	}
}
