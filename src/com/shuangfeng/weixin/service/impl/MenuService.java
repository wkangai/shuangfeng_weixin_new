package com.shuangfeng.weixin.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.shuangfeng.weixin.entity.Menu;
import com.shuangfeng.weixin.service.IMenuService;
import com.shuangfeng.weixin.service.condition.MenuQueryCondition;
import com.shuangfeng.weixin.service.condition.core.Condition;
import com.shuangfeng.weixin.service.core.BaseService;


@Service("menuService")
public class MenuService extends BaseService<Menu, Menu> implements IMenuService{

	
	
	/* (non-Javadoc)
	 */
	public List<Menu> findByCondition(Condition condition) {
		return super.getItems(condition);
	}

	public List<Menu> findAll() {
		return super.getItems(new MenuQueryCondition());
	}

	public void add(Menu menu) {
		super.add(menu);
	}

	public void delete(Menu menu) {
		super.delete(menu);
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.IMenuService#deletes(java.lang.Long[])
	 */
	public void deletes(Long[] ids) {
		MenuQueryCondition condition = new MenuQueryCondition();
		condition.setItemIds(ids);
		List<Menu> menus = super.conditionDAOImpl.getItems(condition);
		for(Menu m: menus){
			this.delete(m);
		}
	}

	public void modify(Menu menu) {
		super.update(menu);
	}

	public Menu find(Long id) {
		return super.find(id);
	}
	
}
