package com.shuangfeng.weixin.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.shuangfeng.weixin.dao.IUserDao;
import com.shuangfeng.weixin.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import com.shuangfeng.weixin.service.IUserService;
import com.shuangfeng.weixin.service.condition.UserQueryCondition;
import com.shuangfeng.weixin.service.core.BaseService;

@Service("userService")
public class UserService extends BaseService<User, User> implements IUserService{	

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.IUserService#changeRemark(java.lang.String, java.lang.Long)
	 */
	@Override
	public void changeRemark(String remark, Long id) {
		User user=super.find(id);
		user.setRemark(remark);
		super.update(user);
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.IUserService#findAll()
	 */
	@Override
	public List<User> findAll() {
		return super.getItems(new UserQueryCondition());
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.IUserService#subscribe(com.shuangfeng.weixin.entity.User)
	 */
	@Override
	public void subscribe(User user) {
		super.add(user);
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.core.BaseService#delete(com.shuangfeng.weixin.entity.core.BaseModel)
	 */
	@Override
	public void delete(User user) {
		super.delete(user);
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.core.BaseService#find(java.lang.Long)
	 */
	@Override
	public User find(Long id) {
		return super.find(id);
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.IUserService#unSubscribe(java.lang.String)
	 */
	@Override
	public void unSubscribe(String openId) {
		IUserDao userDao=(IUserDao)conditionDAOImpl;
		User user=userDao.find(openId);	
		user.setSubscribe(false);
		user.setModifiedDate(new Date());
		userDao.update(user);
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.IUserService#moveTo(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void moveTo(Long id, Long groupId) {
		IUserDao userDao=(IUserDao)conditionDAOImpl;
		userDao.moveTo(id, groupId);		
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.service.IUserService#synchronize(com.shuangfeng.weixin.entity.User)
	 */
	@Override
	public void synchronize(User user) {
		IUserDao userDao=(IUserDao)conditionDAOImpl;
		User user2=userDao.find(user.getOpenId());
		if(user2==null){
			userDao.save(user);
		}
		else {
			cloneUser(user2, user);
			userDao.update(user2);
		}		
	}
	
	public static void cloneUser(User targetUser,User sourceUser){
		targetUser.setCity(sourceUser.getCity());
		targetUser.setCountry(sourceUser.getCountry());
		targetUser.setHeadImgUrl(sourceUser.getHeadImgUrl());
		targetUser.setLanguage(sourceUser.getLanguage());
		targetUser.setNickName(sourceUser.getNickName());
		targetUser.setProvince(sourceUser.getProvince());
		targetUser.setRemark(sourceUser.getRemark());
		targetUser.setUnionId(sourceUser.getUnionId());
	}
}
