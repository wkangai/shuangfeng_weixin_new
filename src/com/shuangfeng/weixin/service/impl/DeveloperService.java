package com.shuangfeng.weixin.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.shuangfeng.weixin.entity.Developer;
import com.shuangfeng.weixin.service.IDeveloperService;
import com.shuangfeng.weixin.service.condition.DeveloperQueryCondition;
import com.shuangfeng.weixin.service.core.BaseService;

@Service("developerService")
public class DeveloperService extends BaseService<Developer, Developer>
		implements IDeveloperService {

	public List<Developer> findAll() {
		return super.getItems(new DeveloperQueryCondition());
	}

	public void add(Developer t) {
		super.add(t);
	}

	public void delete(Developer t) {
		super.delete(t);
	}

	public void modify(Developer t) {
		super.update(t);
	}

	public Developer find(Long id) {
		return super.find(id);
	}

}
