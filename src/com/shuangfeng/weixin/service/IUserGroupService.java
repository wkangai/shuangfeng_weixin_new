package com.shuangfeng.weixin.service;

import java.util.List;

import com.shuangfeng.weixin.entity.UserGroup;

/**
 * @author pc
 *
 */
public interface IUserGroupService {
	
	/**
	 * @param groups
	 * 同步分组信息
	 */
	void synchronize(List<UserGroup> groups);
	
	/**
	 * @param group
	 */
	void add(UserGroup group);
	
	
	/**
	 * @return
	 * 获取所有分组
	 */
	List<UserGroup> findAll();
	
	
	/**
	 * @param id
	 * @return 根据id获取分组
	 */
	UserGroup find(Long id);
	
	
	/**
	 * @param groupId
	 * @return 根据groupId获取分组
	 */
	UserGroup find(int groupId);
}
