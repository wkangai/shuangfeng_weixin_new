package com.shuangfeng.weixin.service;

import java.util.List;
import com.shuangfeng.weixin.entity.*;

/**
 * @author pc
 *
 */
public interface IUserService {
	/**
	 * @return 用户列表
	 */
	List<User> findAll();	
	
	/**
	 * @param 添加用户
	 */
	void subscribe(User user);	
	
	/**
	 * @param openId
	 */
	void unSubscribe(String openId);
	
	/**
	 * @param remark
	 * @param id
	 */
	void changeRemark(String remark, Long id);
	
	/**
	 * @param id
	 * @return 指定用户
	 */
	User find(Long id);
	
	/**
	 * @param id
	 * @param groupId
	 * @return 移动成功返回True
	 */
	void moveTo(Long id,Long groupId);	
	
	void synchronize(User user);
}
