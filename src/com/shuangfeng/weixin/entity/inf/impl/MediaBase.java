package com.shuangfeng.weixin.entity.inf.impl;

import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.entity.inf.IMedia;

public abstract class MediaBase extends BaseModel implements IMedia {

	protected String mediaIdOut;
	
	public String getMediaId() {
		return mediaIdOut;
	}


}
