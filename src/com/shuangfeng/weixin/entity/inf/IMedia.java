package com.shuangfeng.weixin.entity.inf;

import com.shuangfeng.weixin.entity.dist.MediaType;

public interface IMedia extends IMediaIdAble {

	MediaType getType();
	String getDescription();
	
}
