package com.shuangfeng.weixin.entity.inf;

public interface IMediaIdAble {

	/**
	 * 上传到微信媒体库，返回的 mediaId
	 * @return
	 */
	String getMediaId();
	
}
