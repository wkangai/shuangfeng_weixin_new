package com.shuangfeng.weixin.entity.inf;

import com.shuangfeng.weixin.entity.dist.RadioFormat;

public interface IReceiveRadio extends IMediaIdAble {

	/**
	 * 
	 * @return
	 */
	RadioFormat getFormat();
	
}
