package com.shuangfeng.weixin.entity.inf;

import com.shuangfeng.weixin.entity.dist.MsgType;

public interface IMessage {

	/**
	 * 
	 * @return msg send from 
	 */
	IWeixinAccount getFrom();
	/**
	 * 
	 * @return msg send to 
	 */
	IWeixinAccount getTo();
	/**
	 * 
	 * @return msg type
	 */
	MsgType getMsgType();
	/**
	 * 
	 * @return true -- in ,false -- out
	 */
	boolean inOut();
	
}
