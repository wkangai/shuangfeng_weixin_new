package com.shuangfeng.weixin.entity.inf;

public interface IResponseMusic {

	/**
	 * 
	 * @return
	 */
	String getTitle();
	
	String getDesc();
	
	String getUrl();
	
	String getHQMusicUrl();
	
	String getThumbMediaId();
	
}
