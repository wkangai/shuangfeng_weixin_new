package com.shuangfeng.weixin.entity.inf;

public interface IResponseTextPic {

	/**
	 * 
	 * @return
	 */
	String getTitle();
	
	String getDesc();
	
	String getUrl();
	
	
}
