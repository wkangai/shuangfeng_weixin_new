package com.shuangfeng.weixin.entity.inf;

import com.shuangfeng.weixin.entity.dist.AccountType;

public interface IWeixinAccount {

	AccountType getType();
	Long getId();
	
}
