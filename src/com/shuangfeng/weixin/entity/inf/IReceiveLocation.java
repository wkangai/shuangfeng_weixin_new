package com.shuangfeng.weixin.entity.inf;

public interface IReceiveLocation {

	/**
	 * 
	 * @return
	 */
	Double getLocationX();
	
	Double getLocationY();
	
	int getScale();
	
	String getLabelMsg();
	
}
