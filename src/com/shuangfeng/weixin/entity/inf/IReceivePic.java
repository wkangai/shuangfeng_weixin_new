package com.shuangfeng.weixin.entity.inf;

public interface IReceivePic extends IMediaIdAble {

	/**
	 * 
	 * @return
	 */
	String getPicUrl();
	
}
