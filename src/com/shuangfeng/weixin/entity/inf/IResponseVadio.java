package com.shuangfeng.weixin.entity.inf;

public interface IResponseVadio extends IMediaIdAble {

	/**
	 * 
	 * @return
	 */
	String getTitle();
	
	String getDesc();
	
}
