package com.shuangfeng.weixin.entity.inf;

public interface IReceiveLink {

	/**
	 * 
	 * @return
	 */
	String getTitle();
	
	String getDesc();
	
	String getUrl();
	
}
