package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IReceiveVadio;

public class ReceiveVadio extends ReceiveBase implements IReceiveVadio {

	private String mediaId;

	private String thumbMediaId;
	
	public String getMediaId() {
		return mediaId;
	}


	public String getThumbMediaId() {
		return this.thumbMediaId;
	}


	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}


	/**
	 * @param thumbMediaId the thumbMediaId to set
	 */
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}


	
}
