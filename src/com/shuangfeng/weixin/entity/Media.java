package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.dist.MediaType;
import com.shuangfeng.weixin.entity.inf.impl.MediaBase;

public class Media extends MediaBase {

	private MediaType mediaType = MediaType.NOTHING;
	private String description;
	
	public MediaType getType() {
		return this.mediaType;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * @return the mediaType
	 */
	public MediaType getMediaType() {
		return mediaType;
	}

	/**
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
}
