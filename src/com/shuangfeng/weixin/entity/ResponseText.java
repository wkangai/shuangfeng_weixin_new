package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IReceiveText;

public class ResponseText extends ResponseBase implements IReceiveText {

	private String content;
	
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	
}
