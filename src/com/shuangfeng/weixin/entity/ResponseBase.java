package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.entity.dist.MsgType;
import com.shuangfeng.weixin.entity.inf.IMessageResponse;
import com.shuangfeng.weixin.entity.inf.IWeixinAccount;

public class ResponseBase extends BaseModel implements IMessageResponse {

	protected IWeixinAccount developer;
	protected IWeixinAccount user;
	protected String userOpenId;
	protected int msgType = MsgType.NOTHING.getValue();
	protected Long msgId;
	
	public IWeixinAccount getFrom() {
		return developer;
	}

	public IWeixinAccount getTo() {
		return user;
	}

	public MsgType getMsgType() {
		return MsgType.getObject(this.msgType);
	}

	public boolean inOut() {
		return false;
	}

	/**
	 * @return the developer
	 */
	public IWeixinAccount getDeveloper() {
		return developer;
	}

	/**
	 * @param developer the developer to set
	 */
	public void setDeveloper(IWeixinAccount developer) {
		this.developer = developer;
	}

	/**
	 * @return the user
	 */
	public IWeixinAccount getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(IWeixinAccount user) {
		this.user = user;
	}

	/**
	 * @return the userOpenId
	 */
	public String getUserOpenId() {
		return userOpenId;
	}

	/**
	 * @param userOpenId the userOpenId to set
	 */
	public void setUserOpenId(String userOpenId) {
		this.userOpenId = userOpenId;
	}

	/**
	 * @return the msgId
	 */
	public Long getMsgId() {
		return msgId;
	}

	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(MsgType msgType) {
		this.msgType = msgType.getValue();
	}
	
	

}
