package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IResponseRadio;

public class ResponseRadio extends ResponseBase implements IResponseRadio {

	private String mediaId;
	
	public String getMediaId() {
		return this.mediaId;
	}

	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	
}
