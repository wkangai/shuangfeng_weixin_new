package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IReceivePic;

public class ReceivePic extends ReceiveBase implements IReceivePic {

	private String mediaId;
	private String picUrl;

	public String getMediaId() {
		return mediaId;
	}

	public String getPicUrl() {
		return picUrl;
	}

	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * @param picUrl the picUrl to set
	 */
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	
}
