package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.dist.RadioFormat;
import com.shuangfeng.weixin.entity.inf.IReceiveRadio;

public class ReceiveRadio extends ReceiveBase implements IReceiveRadio {

	private String mediaId;

	private RadioFormat format;
	
	public String getMediaId() {
		return mediaId;
	}

	public RadioFormat getFormat() {
		return format;
	}

	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(RadioFormat format) {
		this.format = format;
	}


	
}
