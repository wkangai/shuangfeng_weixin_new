package com.shuangfeng.weixin.entity;


import com.shuangfeng.weixin.common.dist.IEnum;
import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.entity.dist.AccountType;
import com.shuangfeng.weixin.entity.inf.IWeixinAccount;
/**
 * 开发者账号
 * @author pc
 *
 */
public class Developer extends BaseModel implements IWeixinAccount {

	private String appid;
	private String appSecrect;
	private String accessToken;
	private String token;
	/**
	 * 状态
	 */
	private int state = DeveloperState.NOT_ACTIVE.getValue();
	
	/**
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}

	public AccountType getType() {
		return AccountType.DEVELOPER;
	}

	public Long getId() {
		return super.id;
	}

	/**
	 * @return the appid
	 */
	public String getAppid() {
		return appid;
	}

	/**
	 * @param appid the appid to set
	 */
	public void setAppid(String appid) {
		this.appid = appid;
	}

	/**
	 * @return the appSecrect
	 */
	public String getAppSecrect() {
		return appSecrect;
	}

	/**
	 * @param appSecrect the appSecrect to set
	 */
	public void setAppSecrect(String appSecrect) {
		this.appSecrect = appSecrect;
	}

	/**
	 * @return the accessToken
	 */
	public String getAccessToken() {
		return accessToken;
	}

	/**
	 * @param accessToken the accessToken to set
	 */
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	
	public enum DeveloperState implements IEnum<DeveloperState> {

		NOT_ACTIVE(0,"未激活"),
		ACTIVE(1,"已激活");
		
		private int value;
		private String name;
		
		private DeveloperState(int value, String name) {
			this.value = value;
			this.name = name;
		}

		/**
		 * @return the value
		 */
		public int getValue() {
			return value;
		}

		/**
		 * @param value the value to set
		 */
		public void setValue(int value) {
			this.value = value;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		public static DeveloperState get(int value) {
			switch(value){
			case 0: return NOT_ACTIVE;
			case 1: return ACTIVE;
			}
			return null;
		}

		public DeveloperState getEnum(int value) {
			return get(value);
		}

		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return this.getName();
		}
	
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.entity.core.BaseModel#getVersion()
	 */
	@Override
	public int getVersion() {
		// TODO Auto-generated method stub
		return this.version;
	}

	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.entity.core.BaseModel#setVersion(int)
	 */
	@Override
	public void setVersion(int version) {
		// TODO Auto-generated method stub
		this.version = version;
	}
	
	
}
