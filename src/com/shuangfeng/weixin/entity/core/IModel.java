package com.shuangfeng.weixin.entity.core;

import java.util.Date;

public interface IModel extends java.io.Serializable {

	Long getId();
	int getVersion();
	Date getCreatedDate();
	Date getModifiedDate();
	
}
