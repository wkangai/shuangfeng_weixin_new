package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IResponsePic;

public class ResponsePic extends ResponseBase implements IResponsePic {

	private String mediaId;
	
	public String getMediaId() {
		return this.mediaId;
	}

	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	
}
