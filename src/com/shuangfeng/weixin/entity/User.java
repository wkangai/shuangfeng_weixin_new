package com.shuangfeng.weixin.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.shuangfeng.weixin.common.dist.IEnum;
import com.shuangfeng.weixin.entity.Developer.DeveloperState;
import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.entity.dist.AccountType;
import com.shuangfeng.weixin.entity.inf.IWeixinAccount;

public class User extends BaseModel implements IWeixinAccount {

	public User(){
		version=0;
		createdDate=new Date();
		modifiedDate=new Date();
		subscribe=true;
	}
	
	 private String openId;
	 private Boolean subscribe;
	 private String nickName;
	 /**
	  * 暂定男性=1，女性=0，之后改成枚举类型。
	  */
	 private short sex;     
	 private String city;
	 private String country;
	 private String province;
	 private String language;
	 private String headImgUrl;
	 @DateTimeFormat(pattern="yyyy-MM-dd")
	 private Date subscribeTime;
	 private String unionId;
	 private String remark;
	 private UserGroup group;

	
	public AccountType getType() {
		return AccountType.USER;
	}


	/**
	 * @return the openId
	 */
	public String getOpenId() {
		return openId;
	}


	/**
	 * @param openId the openId to set
	 */
	public void setOpenId(String openId) {
		this.openId = openId;
	}


	/**
	 * @return the subscribe
	 */
	public Boolean getSubscribe() {
		return subscribe;
	}


	/**
	 * @param subscribe the subscribe to set
	 */
	public void setSubscribe(Boolean subscribe) {
		this.subscribe = subscribe;
	}


	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}


	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}


	/**
	 * @return the sex
	 */
	public short getSex() {
		return sex;
	}


	/**
	 * @param sex the sex to set
	 */
	public void setSex(short sex) {
		this.sex = sex;
	}


	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}


	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}


	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}


	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}


	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}


	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}


	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}


	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}


	/**
	 * @return the headImgUrl
	 */
	public String getHeadImgUrl() {
		return headImgUrl;
	}


	/**
	 * @param headImgUrl the headImgUrl to set
	 */
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}


	/**
	 * @return the subscribeTime
	 */
	public Date getSubscribeTime() {
		return subscribeTime;
	}


	/**
	 * @param subscribeTime the subscribeTime to set
	 */
	public void setSubscribeTime(Date subscribeTime) {
		this.subscribeTime = subscribeTime;
	}


	/**
	 * @return the unionId
	 */
	public String getUnionId() {
		return unionId;
	}


	/**
	 * @param unionId the unionId to set
	 */
	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}


	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}


	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}


	/**
	 * @return the group
	 */
	public UserGroup getGroup() {
		return group;
	}


	/**
	 * @param group the group to set
	 */
	public void setGroup(UserGroup group) {
		this.group = group;
	}
}
