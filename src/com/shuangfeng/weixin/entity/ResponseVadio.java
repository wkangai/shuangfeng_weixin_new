package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IResponseVadio;

public class ResponseVadio extends ResponseBase implements IResponseVadio {

	private String mediaId;
	private String title;
	private String desc;
	
	public String getMediaId() {
		return this.mediaId;
	}

	/**
	 * @param mediaId the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}


	
}
