package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IReceiveLocation;

public class ReceiveLocation extends ReceiveBase implements IReceiveLocation {

	private Double locationX;
	private Double locationY;
	private int scale;
	private String labelMsg;
	
	/**
	 * @return the locationX
	 */
	public Double getLocationX() {
		return locationX;
	}
	/**
	 * @param locationX the locationX to set
	 */
	public void setLocationX(Double locationX) {
		this.locationX = locationX;
	}
	/**
	 * @return the locationY
	 */
	public Double getLocationY() {
		return locationY;
	}
	/**
	 * @param locationY the locationY to set
	 */
	public void setLocationY(Double locationY) {
		this.locationY = locationY;
	}
	/**
	 * @return the scale
	 */
	public int getScale() {
		return scale;
	}
	/**
	 * @param scale the scale to set
	 */
	public void setScale(int scale) {
		this.scale = scale;
	}
	/**
	 * @return the labelMsg
	 */
	public String getLabelMsg() {
		return labelMsg;
	}
	/**
	 * @param labelMsg the labelMsg to set
	 */
	public void setLabelMsg(String labelMsg) {
		this.labelMsg = labelMsg;
	}
	

}
