package com.shuangfeng.weixin.entity;



import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.entity.dist.MenuResponseType;

public class Menu extends BaseModel {

	private int type = MenuResponseType.NOTHING.getValue();
	private String name;
	private String keyName;
	private String url;
	private Long parentId;
	private Menu parent;
	
//	private Set<Menu> childrens;
	
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * 
	 * @return
	 */
	public String getType_Code() {
		return MenuResponseType.getObject(this.getType()).getCode();
	}
	/**
	 * 
	 * @return
	 */
	public String getType_Name(){
		return MenuResponseType.getObject(this.getType()).getName();
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the keyName
	 */
	public String getKeyName() {
		return keyName;
	}
	/**
	 * @param keyName the keyName to set
	 */
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the parentId
	 */
	public Long getParentId() {
		return this.getParent()!=null ? this.getParent().getId() : 0L;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	/**
	 * @return the parent
	 */
	public Menu getParent() {
		return parent;
	}
	/**
	 * @param parent the parent to set
	 */
	public void setParent(Menu parent) {
		this.parent = parent;
	}
	
	
	
}
