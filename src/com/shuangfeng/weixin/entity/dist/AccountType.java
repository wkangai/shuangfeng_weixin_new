package com.shuangfeng.weixin.entity.dist;

import com.shuangfeng.weixin.common.dist.IEnum;

/**
 * wei xin account type 
 * @author pc
 *
 */
public enum AccountType implements IEnum<AccountType>{

	NOTHING(0,""),
	/**
	 * 开发者账号
	 */
	DEVELOPER(1,"开发者账号"),
	/**
	 * 普通用户账号
	 */
	USER(2,"用户");
	
	private int value;
	private String name;
	
	private AccountType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public static AccountType getObject(int value) {
		for(AccountType e:values()){
			if(e.getValue() == value){
				return e;
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.common.dist.IEnum#getEnum(int)
	 */
	public AccountType getEnum(int value) {
		return getObject(value);
	}
}
