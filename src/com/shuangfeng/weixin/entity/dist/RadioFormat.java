package com.shuangfeng.weixin.entity.dist;

import com.shuangfeng.weixin.common.dist.IEnum;

/**
 * RadioFormat
 * @author pc
 *
 */
public enum RadioFormat implements IEnum<RadioFormat> {

	NOTHING(0,""),
	/**
	 * 图片
	 */
	PICTURE(1,"amr"),
	/**
	 * 
	 */
	RADIO(2,"speex");
	
	private int value;
	private String name;
	
	private RadioFormat(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static RadioFormat getObject(int value) {
		for(RadioFormat e:values()){
			if(e.getValue() == value){
				return e;
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.common.dist.IEnum#getEnum(int)
	 */
	public RadioFormat getEnum(int value) {
		return getObject(value);
	}
	
	
}
