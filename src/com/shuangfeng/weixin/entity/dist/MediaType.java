package com.shuangfeng.weixin.entity.dist;

import com.shuangfeng.weixin.common.dist.IEnum;

/**
 * MediaType
 * @author pc
 *
 */
public enum MediaType implements IEnum<MediaType>{

	NOTHING(0,""),
	/**
	 * 图片
	 */
	PICTURE(1,"图片"),
	/**
	 * 
	 */
	RADIO(2,"语音"),
	/**
	 * 
	 */
	VIDEO(3,"视频"),
	/**
	 * 
	 */
	MUSIC(4,"音乐");
	
	private int value;
	private String name;
	
	private MediaType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public static MediaType getObject(int value) {
		for(MediaType e:values()){
			if(e.getValue() == value){
				return e;
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.shuangfeng.weixin.common.dist.IEnum#getEnum(int)
	 */
	public MediaType getEnum(int value) {
		return getObject(value);
	}
	
}
