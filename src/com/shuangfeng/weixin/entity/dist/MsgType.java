package com.shuangfeng.weixin.entity.dist;

import com.shuangfeng.weixin.common.dist.IEnum;

/**
 * MsgType 
 * @author pc
 *
 */
public enum MsgType implements IEnum<MsgType>{

	NOTHING(0,""),
	/**
	 * 文本消息
	 */
	TEXT(1,"文本消息"),
	/**
	 * 图片消息
	 */
	PICTURE(2,"图片消息"),
	/**
	 * 
	 */
	RADIO(3,"语音消息"),
	/**
	 * 
	 */
	VIDEO(4,"视频消息"),
	/**
	 * 
	 */
	LOCATION(5,"地理位置消息"),
	/**
	 * 
	 */
	LINK(6,"链接消息"),
	/**
	 * 
	 */
	MUSIC(7,"音乐消息"),
	/**
	 * 图文消息
	 */
	PIC_TEXT(8,"图文消息");
	
	private int value;
	private String name;
	
	private MsgType(int value, String name) {
		this.value = value;
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public MsgType getEnum(int value) {
		return getObject(value);
	}
	
	public static MsgType getObject(int value) {
		for(MsgType e:values()){
			if(e.getValue() == value){
				return e;
			}
		}
		return null;
	}
}
