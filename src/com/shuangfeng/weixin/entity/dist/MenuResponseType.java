package com.shuangfeng.weixin.entity.dist;

import com.shuangfeng.weixin.common.dist.IEnum;

public enum MenuResponseType implements IEnum<MenuResponseType>{
	
	NOTHING(0,"",""),
	/**
	 * 点击推事件
	 */
	CLICK(1,"click","点击"),
	/**
	 * 跳转URL
	 */
	VIEW(2,"view","跳转url"),
	/**
	 * 扫码推事件
	 */
	SCANCODE_PUSH(3,"scancode_push","扫码推事件"),
	/**
	 * 扫码推事件且弹出“消息接收中”提示框
	 */
	SCANCODE_WAITMSG(4,"scancode_waitmsg","消息接收中"),
	/**
	 * 弹出系统拍照发图
	 */
	PIC_SYSPHOTO(5,"pic_sysphoto","拍照发图"),
	/**
	 * 弹出拍照或者相册发图
	 */
	PIC_PHOTO_OR_ALBUM(6,"pic_photo_or_album","弹出拍照"),
	/**
	 * 弹出微信相册发图器
	 */
	PIC_WEIXIN(7,"pic_weixin","相册发图器"),
	/**
	 * 弹出地理位置选择器
	 */
	LOCATION_SELECT(8,"location_select","地理位置选择器");
	
	private int value;
	private String code;
	private String name;
	
	private MenuResponseType(int value, String code, String name) {
		this.value = value;
		this.code = code;
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	public static MenuResponseType getObject(int value) {
		for(MenuResponseType e:values()){
			if(e.getValue() == value){
				return e;
			}
		}
		return null;
	}
	
	public MenuResponseType getEnum(int value) {
		return getObject(value);
	}
	
	
}
