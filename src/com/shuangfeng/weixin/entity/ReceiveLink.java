package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.inf.IReceiveLink;

public class ReceiveLink extends ReceiveBase implements IReceiveLink {

	private String title;
	private String description;
	private String url;
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return description;
	}
	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.description = desc;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	

}
