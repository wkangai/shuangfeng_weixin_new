package com.shuangfeng.weixin.entity;

import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.entity.dist.MsgType;
import com.shuangfeng.weixin.entity.inf.IMessageRecerve;
import com.shuangfeng.weixin.entity.inf.IWeixinAccount;

public class ReceiveBase extends BaseModel implements IMessageRecerve {

	protected IWeixinAccount user;
	protected String userOpenId;
	protected IWeixinAccount developer;
	protected int msgType = MsgType.NOTHING.getValue();
	protected Long msgId;
	
	public IWeixinAccount getFrom() {
		return user;
	}

	public IWeixinAccount getTo() {
		return developer;
	}


	public boolean inOut() {
		return true;
	}

	public Long getMsgId() {
		return this.msgId;
	}

	/**
	 * @return the user
	 */
	public IWeixinAccount getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(IWeixinAccount user) {
		this.user = user;
	}

	/**
	 * @return the userOpenId
	 */
	public String getUserOpenId() {
		return userOpenId;
	}

	/**
	 * @param userOpenId the userOpenId to set
	 */
	public void setUserOpenId(String userOpenId) {
		this.userOpenId = userOpenId;
	}

	/**
	 * @return the developer
	 */
	public IWeixinAccount getDeveloper() {
		return developer;
	}

	/**
	 * @param developer the developer to set
	 */
	public void setDeveloper(IWeixinAccount developer) {
		this.developer = developer;
	}

	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}


	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(MsgType msgType) {
		this.msgType = msgType.getValue();
	}

	public MsgType getMsgType() {
		return MsgType.getObject(this.msgType);
	}
	
	
}
