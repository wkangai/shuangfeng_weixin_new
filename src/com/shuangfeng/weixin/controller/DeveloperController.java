package com.shuangfeng.weixin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.shuangfeng.weixin.common.util.JsonSerializer;
import com.shuangfeng.weixin.controller.core.BaseController;
import com.shuangfeng.weixin.controller.models.JsonModel;
import com.shuangfeng.weixin.controller.models.JsonModel.JsonModelStatus;
import com.shuangfeng.weixin.entity.Developer;
import com.shuangfeng.weixin.service.IDeveloperService;

@Controller
public class DeveloperController extends BaseController {

	@Autowired
	private IDeveloperService developerService;
	
	@RequestMapping("developer/show.jhtml")
	public ModelAndView list(HttpSession session, HttpServletResponse response,
			HttpServletRequest request) {
		Developer d = developerService.findAll().get(0);
		request.setAttribute("developer", d);
		return new ModelAndView("developer/show");
	}
	
	@RequestMapping("developer/modify.jhtml")
	public ModelAndView modify(HttpServletRequest request) {
		Developer d = developerService.find(super.getModifyId(request));
		request.setAttribute("developer", d);
		return new ModelAndView("developer/modify");
	}
	
	@RequestMapping("developer/doModify.jhtml")
	public @ResponseBody String doModify(HttpServletRequest request,@ModelAttribute("Developer") Developer developer) throws Exception{
		JsonModel jsonModel =new JsonModel();
		try {
			developerService.modify(developer);
			jsonModel.setStatus(JsonModelStatus.SUCCESS);
			jsonModel.setMessage("保存成功!");
		} catch (Exception e) {
			jsonModel.setStatus(JsonModelStatus.FAIL);
			jsonModel.setMessage("保存失败!");
		}
    	return JsonSerializer.SerializeObject(jsonModel);
	}
	
}
