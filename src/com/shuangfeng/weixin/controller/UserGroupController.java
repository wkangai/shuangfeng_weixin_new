package com.shuangfeng.weixin.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.shuangfeng.weixin.common.util.JsonSerializer;
import com.shuangfeng.weixin.controller.core.BaseController;
import com.shuangfeng.weixin.controller.models.JsonModel;
import com.shuangfeng.weixin.controller.models.JsonModel.JsonModelStatus;
import com.shuangfeng.weixin.entity.User;
import com.shuangfeng.weixin.entity.UserGroup;
import com.shuangfeng.weixin.service.IUserGroupService;
import com.shuangfeng.weixin.service.IUserService;

@RequestMapping("group")
@Controller
public class UserGroupController extends BaseController {
	
	@Resource(name="userGroupService")
	private IUserGroupService groupService;
	
	@RequestMapping("/list.jhtml")
	public ModelAndView getList(){
		List<UserGroup> groups=groupService.findAll();
		UserGroup group=new UserGroup();
		group.setGroupId(0);
		group.setName("未分组");
		group.setCount(0);
		group.setId(0L);
		groups.add(0, group);
		ModelAndView model=new ModelAndView();
		model.setViewName("userGroup/list");
		model.addObject("groups", groups);
		return model;
	}
	
	@RequestMapping(value="/add.jhtml",method=RequestMethod.POST)
	public @ResponseBody String add(UserGroup group) throws JsonProcessingException{
		JsonModel<User> jsonModel=new JsonModel<User>();
		try {
			groupService.add(group);
			jsonModel.setStatus(JsonModelStatus.SUCCESS);
			jsonModel.setMessage("保存成功!");
		} catch (Exception e) {
			jsonModel.setStatus(JsonModelStatus.FAIL);
			jsonModel.setMessage("保存失败!");
		}
		return JsonSerializer.SerializeObject(jsonModel);
	}
	
	@RequestMapping("/detail.jhtml")
	public ModelAndView detail(Long id){
		UserGroup group=groupService.find(id);
		ModelAndView model=new ModelAndView();
		model.addObject("group", group);
		model.setViewName("userGroup/detail");
		return model;
	}
}
