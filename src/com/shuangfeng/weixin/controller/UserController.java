package com.shuangfeng.weixin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.mapping.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.shuangfeng.weixin.common.util.JsonSerializer;
import com.shuangfeng.weixin.controller.core.BaseController;
import com.shuangfeng.weixin.controller.models.JsonModel;
import com.shuangfeng.weixin.controller.models.JsonModel.JsonModelStatus;
import com.shuangfeng.weixin.service.IUserService;
import com.shuangfeng.weixin.entity.User;

@Controller
public class UserController extends BaseController {
	
	@Resource(name="userService")
	private IUserService userService;
	
	@RequestMapping("user/list.jhtml")
	public ModelAndView list(){
		List<User> users=userService.findAll();
		ModelAndView model=new ModelAndView("user/list");
		model.addObject("users", users);
		return model;
	}
	
	@RequestMapping(value="user/changeRemark",method=RequestMethod.POST)
	public @ResponseBody String changeRemark(String remark, Long id) throws JsonProcessingException{
		JsonModel<User> jsonModel=new JsonModel<User>();
		try {
			userService.changeRemark(remark, id);
			jsonModel.setStatus(JsonModelStatus.SUCCESS);
			jsonModel.setMessage("保存成功!");
		} catch (Exception e) {
			jsonModel.setStatus(JsonModelStatus.FAIL);
			jsonModel.setMessage("保存失败!");
		}
		return JsonSerializer.SerializeObject(jsonModel);
	}
	
	@RequestMapping(value="user/add.jhtml",method=RequestMethod.POST)
	public String addUser(@ModelAttribute("user") User user){
		userService.subscribe(user);
		return "user/list";
	}
}
