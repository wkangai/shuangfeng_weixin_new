package com.shuangfeng.weixin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.shuangfeng.weixin.api.model.Button;
import com.shuangfeng.weixin.api.trans.WeiXinTransfer;
import com.shuangfeng.weixin.common.util.JsonSerializer;
import com.shuangfeng.weixin.controller.core.BaseController;
import com.shuangfeng.weixin.controller.models.JsonModel;
import com.shuangfeng.weixin.controller.models.JsonModel.JsonModelStatus;
import com.shuangfeng.weixin.entity.Menu;
import com.shuangfeng.weixin.service.IMenuService;
import com.shuangfeng.weixin.service.condition.MenuQueryCondition;
import com.shuangfeng.weixin.service.condition.core.Condition;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Controller
public class MenuController extends BaseController {

	@Autowired
	private IMenuService menuService;
	
	@RequestMapping("menu/list.jhtml")
	public ModelAndView list(HttpSession session, HttpServletResponse response,
			HttpServletRequest request) {
		List<Menu> all = menuService.findAll();
		request.setAttribute("menus", all);
		return new ModelAndView("menu/list");
	}
	
	@RequestMapping("menu/add.jhtml")
	public ModelAndView add(HttpServletRequest request) {
		List<Menu> all = menuService.findAll();
		request.setAttribute("menus", all);
		return new ModelAndView("menu/add");
	}
	
	@RequestMapping("menu/doAdd.jhtml")
	public @ResponseBody String doAdd(HttpServletRequest request,@ModelAttribute("Menu") Menu menu) throws JsonProcessingException{
		JsonModel jsonModel =new JsonModel();
		try {
			menuService.add(menu);
			jsonModel.setStatus(JsonModelStatus.SUCCESS);
			jsonModel.setMessage("保存成功!");
		} catch (Exception e) {
			jsonModel.setStatus(JsonModelStatus.FAIL);
			jsonModel.setMessage("保存失败!");
		}
		return JsonSerializer.SerializeObject(jsonModel);
	}
	
	@RequestMapping("menu/modify.jhtml")
	public ModelAndView modify(HttpServletRequest request) {
		Menu m = menuService.find(super.getModifyId(request));
		List<Menu> all = menuService.findAll();
		request.setAttribute("menus", all);
		request.setAttribute("menu", m);
		return new ModelAndView("menu/modify");
	}
	
	@RequestMapping("menu/doModify.jhtml")
	public @ResponseBody String doModify(HttpServletRequest request,@ModelAttribute("Menu") Menu menu) throws Exception{
		JsonModel jsonModel =new JsonModel();
		try {
			menuService.modify(menu);
			jsonModel.setStatus(JsonModelStatus.SUCCESS);
			jsonModel.setMessage("保存成功!");
		} catch (Exception e) {
			jsonModel.setStatus(JsonModelStatus.FAIL);
			jsonModel.setMessage("保存失败!");
		}
    	return JsonSerializer.SerializeObject(jsonModel);
	}
	
	@RequestMapping("menu/doDelete.jhtml")
	public @ResponseBody String delete(HttpServletRequest request) throws JsonProcessingException {
		JsonModel jsonModel =new JsonModel();
		try {
		this.menuService.deletes(super.getGeneralSelectedIds(request));
			jsonModel.setStatus(JsonModelStatus.SUCCESS);
			jsonModel.setMessage("删除成功!");
		} catch (Exception e) {
			jsonModel.setStatus(JsonModelStatus.FAIL);
			jsonModel.setMessage("删除失败!");
		}
    	return JsonSerializer.SerializeObject(jsonModel);
	}
	
	@RequestMapping("menu/doSync.jhtml")
	public @ResponseBody String doSync(HttpServletRequest request) throws JsonProcessingException {
		JsonModel jsonModel =new JsonModel();
		List<Menu> all = this.menuService.findAll();
		return WeiXinTransfer.syncMenu(all);
	}
	
	
}
