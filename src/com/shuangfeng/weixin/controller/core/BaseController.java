package com.shuangfeng.weixin.controller.core;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.shuangfeng.weixin.common.util.ArrayUtil;


public abstract class BaseController {

	/**
	 * 搜索关键字key
	 */
	public static final String DEFAULT_KEYWORDS_PARAM = "_general_keywords";
	/**
	 * 默认删除id参数名
	 */
	public static final String DEFAULT_GENERAL_IDS_PARAM = "_general_param_ids";
	/**
	 * 默认修改id参数名
	 */
	public static final String DEFAULT_MODIFY_IDS_PARAM = "_modify_param_id";
	/**
	 * session 中用户的参数名
	 */
	public static final String SESSION_USER_PARAM="user";
	
	/**
	 * 获得页面查询关键字
	 * @param request
	 * @return
	 */
	protected String getGeneralKeywords(HttpServletRequest request){
		return request.getParameter(DEFAULT_KEYWORDS_PARAM);
	}
	
	/**
	 * 获得页面修改的id
	 * 页面修改参数必须是 "_modify_param_id"
	 * @param request
	 * @return 修改的id
	 */
	protected Long getModifyId(HttpServletRequest request){
		return getModifyId(request,DEFAULT_MODIFY_IDS_PARAM);
	}
	
	/**
	 * 
	 * @param request
	 * @param id_param_name
	 * @return
	 */
	protected Long getModifyId(HttpServletRequest request,
			String id_param_name) {
		String id = request.getParameter(id_param_name);
		return Long.valueOf(id);
	}

	/**
	 * 获得页面选择的id
	 * 页面选择参数必须是 "_general_param_ids"
	 * @param request
	 * @return 选择的id / null
	 */
	protected Long[] getGeneralSelectedIds(HttpServletRequest request){
		return getGeneralSelectedIds(request,DEFAULT_GENERAL_IDS_PARAM);
	}
	
	/**
	 * 获得选择的ids
	 * @param request
	 * @param id_param_name
	 * @return 选择的id / null
	 */
	protected Long[] getGeneralSelectedIds(HttpServletRequest request,String id_param_name){
		String ids = request.getParameter(id_param_name);
		if(!StringUtils.isEmpty(ids)){
			String[] idArray = ids.contains(",")?ids.split(","):new String[]{ids};
			return ArrayUtil.String2Long(idArray);
		}
		return null;
	}
	
}
