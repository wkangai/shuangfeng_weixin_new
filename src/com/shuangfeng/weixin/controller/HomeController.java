package com.shuangfeng.weixin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.shuangfeng.weixin.service.IMenuService;

@Controller("/home")
public class HomeController {

	@Autowired
	private IMenuService menuService;
	
	@RequestMapping("home.jhtml")
	public ModelAndView Home(HttpSession session, HttpServletResponse response,
			HttpServletRequest request) {
		return new ModelAndView("home");
	}
	
}
