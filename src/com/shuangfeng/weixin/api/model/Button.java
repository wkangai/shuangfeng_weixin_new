package com.shuangfeng.weixin.api.model;

import java.util.List;

/**
 * 自定义菜单
 * @author pc
 *
 */
public class Button {

	private List<WXMenu> button;

	/**
	 * @return the button
	 */
	public List<WXMenu> getButton() {
		return button;
	}

	/**
	 * @param button the button to set
	 */
	public void setButton(List<WXMenu> button) {
		this.button = button;
	}
	
}
