package com.shuangfeng.weixin.api.trans;

import java.util.ArrayList;
import java.util.List;

import com.shuangfeng.weixin.api.model.Button;
import com.shuangfeng.weixin.api.model.WXMenu;
import com.shuangfeng.weixin.entity.Menu;
/**
 * weixin menu data transfer
 * @author pc
 *
 */
public class WXMenuParser {

	protected static Button parseData(List<Menu> menus){
		Button button = new Button();
		List<WXMenu> wxMenus = new ArrayList<WXMenu>();
		for(Menu m:menus){
			WXMenu w = new WXMenu();
			w.setType(m.getType_Code());
			w.setKey(m.getKeyName());
			w.setName(m.getName());
			if(m.getParentId()==null || m.getParentId()==0){
				fillChild(m,menus,w);
				wxMenus.add(w);
			}
		}
		button.setButton(wxMenus);
		
		return ruleParent(button);
	}
	
	private static Button ruleParent(Button button){
		List<WXMenu> menus = button.getButton();
		for(WXMenu m:menus){
			if(m.getSub_button()!=null){
				m.setKey(null);
				m.setType(null);
			}
		}
		return button;
	}
	
	private static void fillChild(Menu parent,List<Menu> data,WXMenu wxMenu){
		List<WXMenu> ms = wxMenu.getSub_button();
		for(Menu m:data){
			if(m.getParent()!=null && m.getParent().getId() == parent.getId()){
				WXMenu w = new WXMenu();
				w.setType(m.getType_Code());
				w.setKey(m.getKeyName());
				w.setName(m.getName());
				if(ms == null){
					ms = new ArrayList<WXMenu>();
				}
				fillChild(m,data,w);
				ms.add(w);
			}
		}
		wxMenu.setSub_button(ms);
	}
	
}
