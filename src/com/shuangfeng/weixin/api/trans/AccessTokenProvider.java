package com.shuangfeng.weixin.api.trans;

import com.shuangfeng.weixin.service.IDeveloperService;

public class AccessTokenProvider {

	private IDeveloperService developerService;
	
	public String getAccessToken(){
		return developerService.findAll().get(0).getAccessToken();
	}

	/**
	 * @return the developerService
	 */
	public IDeveloperService getDeveloperService() {
		return developerService;
	}

	/**
	 * @param developerService the developerService to set
	 */
	public void setDeveloperService(IDeveloperService developerService) {
		this.developerService = developerService;
	}
	
}
