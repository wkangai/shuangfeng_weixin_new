package com.shuangfeng.weixin.api.trans;

import java.util.List;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.shuangfeng.weixin.common.util.JsonSerializer;
import com.shuangfeng.weixin.entity.Menu;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

public class WeiXinTransfer {

	public static Client client = Client.create();
	public static String RESOURCE_MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create";
	public static String ACCESS_TOKEN;
	public static AccessTokenProvider accessTokenProvider;
	
	
	/**
	 * @return the ACCESS_TOKEN
	 */
	public static String getACCESS_TOKEN() {
		if(StringUtils.isEmpty(ACCESS_TOKEN)){
			ACCESS_TOKEN = accessTokenProvider.getAccessToken();
		}
		return ACCESS_TOKEN;
	}


	/**
	 * transfer local menu to weixin server
	 * @param menus
	 * @return 
	 * @throws JsonProcessingException 
	 * @throws ClientHandlerException 
	 * @throws UniformInterfaceException 
	 */
	public static String syncMenu(List<Menu> menus) throws UniformInterfaceException, ClientHandlerException, JsonProcessingException {
		String str = JsonSerializer.SerializeObject(WXMenuParser.parseData(menus));
		WebResource r = client.resource(buildResourceStr(RESOURCE_MENU_CREATE));
		String response = r.post(String.class, JsonSerializer.SerializeObject(WXMenuParser.parseData(menus)));
		return response;
	}

	
	private static String buildResourceStr(String str){
		return str+"?access_token="+getACCESS_TOKEN();
	}


	/**
	 * @return the accessTokenProvider
	 */
	public static AccessTokenProvider getAccessTokenProvider() {
		return accessTokenProvider;
	}


	/**
	 * @param accessTokenProvider the accessTokenProvider to set
	 */
	public static void setAccessTokenProvider(
			AccessTokenProvider accessTokenProvider) {
		WeiXinTransfer.accessTokenProvider = accessTokenProvider;
	}
	
	
}
