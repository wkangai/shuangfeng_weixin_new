package com.shuangfeng.weixin.api;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shuangfeng.weixin.entity.Developer;
import com.shuangfeng.weixin.service.IDeveloperService;

@Controller
public class ActiveController {

	public static final String PARAM_SIGNATURE = "signature";
	public static final String PARAM_TIMESTAMP = "timestamp";
	public static final String PARAM_NONCE = "nonce"	;
	public static final String PARAM_ECHOSTR = "echostr";
	
	@Autowired
	private IDeveloperService developerService;
	
	@RequestMapping("active.jhtml")
	public @ResponseBody String active(HttpSession session, HttpServletResponse response,
			HttpServletRequest request) {
		Developer developer = developerService.findAll().get(0);
		String token = developer.getToken();
		if(this.check(token, request.getParameter(PARAM_TIMESTAMP), request.getParameter(PARAM_NONCE),request.getParameter(PARAM_SIGNATURE))){
			return request.getParameter(PARAM_ECHOSTR)+"_"+token;
		}
		return null;
	}
	
	/**
	 * 1. 将token、timestamp、nonce三个参数进行字典序排序
	 * 2. 将三个参数字符串拼接成一个字符串进行sha1加密
	 * 3. 开发者获得加密后的字符串可与signature对比，标识该请求来源于微信
	 * @param token
	 * @param timestamp
	 * @param nonce
	 * @param signature
	 * @return
	 */
	private boolean check(String token,String timestamp,String nonce,String signature){
		return true;
		
	}
}
