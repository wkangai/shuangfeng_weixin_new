package com.shuangfeng.weixin.common.dist;

/**
 * 
 * @author Administrator
 *
 * @param <E>
 */
public interface IEnum <E extends Enum>{

	/**
	 * 根据value 获得对应的枚举对象
	 * @param value 枚举对象的值
	 * @return 枚举对象
	 */
	 E getEnum(int value);
	/**
	 * 获得枚举对象的  value
	 * @return value
	 */
	int getValue();
	
}
