package com.shuangfeng.weixin.common.dist;

import java.math.BigDecimal;

public class Const {

	public final static String SECRECT_CONFIG_FILE = "secrect";//配置文件名，包含数据库相关配置项等
	
	public static final Integer PARAM_NO_INT = -7807;//Integer类型表示参数为空的时候的值
	
	public final static BigDecimal ZERO_AMOUNT = new BigDecimal("0.00");//BigDecimal类型的数值0
	
	public final static int DATEPOINT_NONE = 0;
	public final static int DATEPOINT_TODAY = DATEPOINT_NONE +1;
	public final static int DATEPOINT_THIS_WEEK = DATEPOINT_TODAY +1;
	public final static int DATEPOINT_THIS_MONTH = DATEPOINT_THIS_WEEK +1;
	public final static int DATEPOINT_THREE_MONTH = DATEPOINT_THIS_MONTH +1;
	public final static int DATEPOINT_HALF_YEAR = DATEPOINT_THREE_MONTH +1;
	public final static int DATEPOINT_ONEYEAR = DATEPOINT_HALF_YEAR +1;
	public final static int DATEPOINT_MORE_THAN_ONE_YEAR = DATEPOINT_ONEYEAR +1;
	
}
