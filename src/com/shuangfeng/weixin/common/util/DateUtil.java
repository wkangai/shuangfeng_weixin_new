package com.shuangfeng.weixin.common.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;


public class DateUtil {

	public final static String DATE_FULL = "yyyy-MM-dd HH:mm:ss";
	public final static String DATE_YEAR_MONTH = "yyyy-MM";
	public final static String DATE_YEAR_MONTH_DAY = "yyyy-MM-dd";
	public final static String DATE_YEAR_MONTH_DAY_H_M = "yyyy-MM-dd HH:mm";
	public final static String DATE_MONTH_DAY = "MM.dd";
	public static SimpleDateFormat FMT_DATE = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat FMT_DATE_HOUR = new SimpleDateFormat("yyyy-MM-dd-HH---");
	public static SimpleDateFormat FMT_DATETIME = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static SimpleDateFormat FMT_SHORT_DATE = new SimpleDateFormat("yyMMdd");
	public static SimpleDateFormat FMT_LONG_DATE = new SimpleDateFormat("yyyyMMddHHmmss");
	public static String DATE_REGEXP = "^[1-2]\\d{3}(\\-\\d{1,2}){2}( (\\d{1,2}:){2}\\d{1,2})?";
	private static Pattern DATE_Pattern = Pattern.compile(DATE_REGEXP);

	public final static DecimalFormat nf = new DecimalFormat("###0");
	static {
		nf.setMaximumIntegerDigits(5);
		nf.setMinimumIntegerDigits(4);
	}

	public static String datetime(Date date) {
		return date == null ? null : FMT_DATETIME.format(date);
	}
	
	public static String datetimeSplitNo(Date date) {
		return date == null ? null : FMT_LONG_DATE.format(date);
	}

	public static String dateHour(Date date) {
		return date == null ? null : FMT_DATE_HOUR.format(date);
	}
	
	public static String date(Date date) {
		return date == null ? null : FMT_DATE.format(date);
	}

	/** 
	* 字符串转换成日期 
	* @param str 
	* @return date 
	*/ 
	public static Date StrToDate(String str) {
		Date date = null;
		try {
			date = FMT_DATETIME.parse(str);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return date;
	} 
	/**
     * 获得指定日期的前后日�?t为正表示�?负表示前
     * 
     * @param specifiedDay
     * @return
     * @throws Exception
     */
    public static Date getSpecifiedDay(Date date, int t) {
    	if(date == null) return null;
    	
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, t);
        return c.getTime();
    }
    
    /**
     * 
     * @param specifiedDay
     * @return
     * @throws Exception
     */
    public static Date getSpecifiedDate(Date date,int calendarField, int t) {
    	if(date == null) return null;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, t);
        return c.getTime();
    }

    public static String getExpectedDate(Date date, int t) {
        return datetime(getSpecifiedDay(date, t));
    }

    public static boolean isDateStr(String dateStr) {
   		if(dateStr == null || dateStr.length() == 0) return false;
   		return DATE_Pattern.matcher(dateStr).matches();
    }
    
    /** 
    * 获得指定日期的前�?�� 
    * @param specifiedDay 
    * @return 
    * @throws Exception 
    */ 
    public static String getPreviousDay(String date){
    	Date d = getSpecifiedDay(StrToDate(date), -1);
    	return datetime(d);
    } 
    
    /** 
    * 获得指定日期的后�?�� 
    * @param specifiedDay 
    * @return 
    */ 
    public static String getNextDay(String date){ 
    	Date d = getSpecifiedDay(StrToDate(date), 1);
    	return datetime(d);
    }
    
    public static String getPlsqlToDate(Date date) {
    	String toDate = "to_date('" + datetime(date)+ "','yyyy-mm-dd hh24:mi:ss')";
    	return toDate;
    }
    
    public static String getPlsqlToDate(String date) {
    	return getPlsqlToDate(StrToDate(date));
    }
    
    //时间戳转化为日期
    public static Date getDate(String unixDate) throws Exception {
    	Date date = null;
    	long unixLong = 0;
 	   try {
 		  unixLong = Long.parseLong(unixDate) ;
 		  date = getDate(unixLong) ;
 	   } catch(Exception ex) {
 		   throw ex;
 	   }
 	   return date ;
 	}
 	public static Date getDate(Long unixLong) throws Exception {
 	   SimpleDateFormat fm2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
 	   Date date = null;
 	   try {
 		   date = FMT_DATETIME.parse(fm2.format(unixLong*1000));
 	   } catch(Exception ex) {
 		   throw ex;
 	   }
 	   return date ;
 	}
 	
 	public static String[] getYearMonthDayOfDate(Date date){
 		return getDateYearMonthDay(date).split("-");
 	}
 	
 	public static String getDateYearMonthDay(Date date) {
		return getDateString(date, DATE_YEAR_MONTH_DAY);
	}
 	
 	public static String getDateString(Date date, String pattern) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(date);
	}
 	
 	public static Long getTimestamp(Date date) throws ParseException{
 		return date.getTime()/1000;
 	}
 	
 	public static boolean isRestTime(Date time) {
		int hour = DateUtil.getHourOfDay(time);
		return (hour >= 23 || hour <= 7);
	}

	//24-hour clock
	public static int getHourOfDay(Date time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(time);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static Long[] getDifToNow(Date fromTime) {
		Long[] diff = new Long[3];
		Date now = new Date();
		Long between = now.getTime()-fromTime.getTime();
		Long day = between/(24*60*60*1000);
		Long hour =(between/(60*60*1000)-day*24);
		Long mm =((between/(60*1000))-day*24*60-hour*60);
		diff[0]=day;
		diff[1]=hour;
		diff[2]=mm;
		return diff;
	}

	
	public static int getNowMinute(){
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.MINUTE);
	}
	/**
	 * weather now is system busy time
	 * @return
	 */
	public static boolean isSystemBusyTime() {
		int m = getNowMinute();
		return m > 59 || m < 1;
	}

	/**
	 * get the begin of a day
	 * @param date
	 * @return
	 */
	public static String getBeginOfDay(Date date) {
		return date(date);
	}

	public static Date getBeginDateOfDay(Date date){
		try {
			return FMT_DATE.parse(getBeginOfDay(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * get the end of a day
	 * @param date
	 * @return
	 */
	public static String getEndOfDay(Date date) {
		return date(getSpecifiedDay(date,1));
	}

	public static int getDiffDays(Date endDate,
			Date beginDate) {
		Long between = endDate.getTime()-beginDate.getTime();
		Long day = between/(24*60*60*1000);
		return day.intValue();
	}
	
}
