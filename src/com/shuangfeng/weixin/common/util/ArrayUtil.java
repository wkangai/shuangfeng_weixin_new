package com.shuangfeng.weixin.common.util;


public class ArrayUtil {
	
	/**
	 * transfer String[] to Long[]
	 * @param ss 
	 * @return
	 */
	public static Long[] String2Long(String[] ss){
		Long[] ll = new Long[ss.length];
		for(int i=0;i<ss.length;i++){
			ll[i] = Long.valueOf(ss[i].trim());
		}
		return ll;
	}

	/**
	 * 数组转为逗号分割的字符串
	 * @param oo 数组
	 * @return
	 */
	public static String Array2String(Object[] oo){
		return Array2StringWithSplit(oo,",");
	}
	
	/**
	 * 数组转化为字符串
	 * @param oo 数组
	 * @param split 分割符
	 * @return
	 */
	public static String Array2StringWithSplit(Object[] oo,String split){
		StringBuilder sb = new StringBuilder();
		for(Object o:oo){
			sb.append(o);
			sb.append(split);
		}
		return sb.substring(0, sb.lastIndexOf(split));
	}
	
}
