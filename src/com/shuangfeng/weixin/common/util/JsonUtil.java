package com.shuangfeng.weixin.common.util;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@SuppressWarnings("unchecked")
public class JsonUtil {
	private static final Log logger = LogFactory.getLog(JsonUtil.class);
	
    public static String JsonFromObject(Object obj) throws Exception{
        String rtn = "";
        rtn = getJsonString(obj);
        return rtn;
    }
    
    @SuppressWarnings("unchecked")
    private static String getJsonString(Object obj) throws Exception{
        if (obj == null) {
            return "\"\"";
        }
        if (obj instanceof Number){
            return numberToString((Number) obj);
        }
        if (obj instanceof Boolean) {
            return obj.toString();
        }
        if (obj instanceof String){
            return quote(obj.toString());
        }
        
        if (obj instanceof Map) {
            return mapToString((Map)obj);
        }
        
        //class
        if(obj instanceof Class)
            return quote(((Class)obj).getSimpleName());
        
        //enum
        if(obj instanceof Enum){
        	return quote(obj.toString());
        }
        	
        //数组
        if (obj instanceof Collection || obj.getClass().isArray()) {
            return getJsonArray(obj);
        }
        // 日期
        if (obj instanceof Date) {
            return quote(dateToString((Date)obj));
        }
        return reflectObject(obj);
    }
    
    @SuppressWarnings("unchecked")
    private static String getJsonArray(Object obj) throws Exception{
        if(obj == null)
            return "[]";
        
        StringBuffer sb = new StringBuffer("[");
        if(obj instanceof Collection){
            Iterator iterator = ((Collection)obj).iterator();
            while(iterator.hasNext()){
                if (sb.length() > 1) {
                    sb.append(',');
                }
                Object rowObj = iterator.next();
                sb.append(getJsonString(rowObj));
            }
        }
        if(obj.getClass().isArray()){
            int arrayLength = Array.getLength(obj);
            for(int i = 0; i < arrayLength; i ++){
                if (sb.length() > 1) {
                    sb.append(',');
                }
                Object rowObj = Array.get(obj, i);
                sb.append(getJsonString(rowObj));
            }
        }
        sb.append("]");
        
//        sb.append(obj.);
        return sb.toString();
    }
    
    @SuppressWarnings("unchecked")
    private static String mapToString(Map map) {
        try {
            Iterator keys = map.keySet().iterator();
            StringBuffer sb = new StringBuffer("{");

            while (keys.hasNext()) {
                if (sb.length() > 1) {
                    sb.append(',');
                }
                Object o = keys.next();
                sb.append(quote(o.toString()));
                sb.append(':');
                sb.append(getJsonString(map.get(o)));
            }
            sb.append('}');
            return sb.toString();
        } catch (Exception e) {
        	logger.warn("", e);
            return "";
        }
    }
    
    @SuppressWarnings("unchecked")
    private static String reflectObject(Object bean) {

        Class c = bean.getClass();
        Method[] methods = c.getMethods();
        
        StringBuffer sb = new StringBuffer("{");
        for (int i = 0; i < methods.length; i += 1) {
            try {
                Method method = methods[i];
                String name = method.getName();
                String key = "";
                if (name.startsWith("get")) {
                    key = name.substring(3);
                } else if (name.startsWith("is")) {
                    key = name.substring(2);
                }
                if (key.length() > 0 && Character.isUpperCase(key.charAt(0)) && method.getParameterTypes().length == 0) {
                    if (key.length() == 1) {
                        key = key.toLowerCase();
                    } else if (!Character.isUpperCase(key.charAt(1))) {
                        key = key.substring(0, 1).toLowerCase() + key.substring(1);
                    }
                    
                    Object elementObj = method.invoke(bean, null);
                    if(elementObj instanceof Class)
                        continue;
                    
                    if (sb.length() > 1) {
                        sb.append(',');
                    }
                    
                    sb.append(quote(key));
                    sb.append(':');
                    sb.append(getJsonString(elementObj));
                }
            } catch (Exception e) {
            	logger.warn("", e);
            }
        }
        
        sb.append('}');
        return sb.toString();
    }
    
    private static String dateToString(Date date) throws Exception {
    	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	return date == null?"" : fmt.format(date);
    }

    /**
     * Produce a string from a Number.
     * @param  number A Number
     * @return A String.
     * @throws JSONException If n is a non-finite number.
     */
    private static String numberToString(Number number) throws Exception{
        if (number == null) {
            return "";
        }
        
        testValidity(number);

        String string = number.toString();
        if (string.indexOf('.') > 0 && string.indexOf('e') < 0 && string.indexOf('E') < 0) {
            while (string.endsWith("0")) {
                string = string.substring(0, string.length() - 1);
            }
            if (string.endsWith(".")) {
                string = string.substring(0, string.length() - 1);
            }
        }
        return string;
    }
    
    public static void testValidity(Object o) throws Exception{
        if (o != null) {
            if (o instanceof Double) {
                if (((Double)o).isInfinite() || ((Double)o).isNaN()) {
                    throw new Exception("JSON does not allow non-finite numbers.");
                }
            } else if (o instanceof Float) {
                if (((Float)o).isInfinite() || ((Float)o).isNaN()) {
                    throw new Exception("JSON does not allow non-finite numbers.");
                }
            }
        }
    }
    
    /**
     * Produce a string in double quotes with backslash sequences in all the
     * right places. A backslash will be inserted within </, producing <\/,
     * allowing JSON text to be delivered in HTML. In JSON text, a string 
     * cannot contain a control character or an unescaped quote or backslash.
     * @param string A String
     * @return  A String correctly formatted for insertion in a JSON text.
     */
    public static String quote(String string) {
        if (string == null || string.length() == 0) {
            return "\"\"";
        }

        char         b;
        char         c = 0;
        String       hhhh;
        int          i;
        int          len = string.length();
        StringBuffer sb = new StringBuffer(len + 4);

        sb.append('"');
        for (i = 0; i < len; i += 1) {
            b = c;
            c = string.charAt(i);
            switch (c) {
            case '\\':
            case '"':
                sb.append('\\');
                sb.append(c);
                break;
            case '/':
                if (b == '<') {
                    sb.append('\\');
                }
                sb.append(c);
                break;
            case '\b':
                sb.append("\\b");
                break;
            case '\t':
                sb.append("\\t");
                break;
            case '\n':
                sb.append("\\n");
                break;
            case '\f':
                sb.append("\\f");
                break;
            case '\r':
                sb.append("\\r");
                break;
            default:
                if (c < ' ' || (c >= '\u0080' && c < '\u00a0') ||
                               (c >= '\u2000' && c < '\u2100')) {
                    hhhh = "000" + Integer.toHexString(c);
                    sb.append("\\u" + hhhh.substring(hhhh.length() - 4));
                } else {
                    sb.append(c);
                }
            }
        }
        sb.append('"');
        return sb.toString();
    }
    
}
