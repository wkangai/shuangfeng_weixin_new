package com.shuangfeng.weixin.common.util;

public class PageUtil {

	/**
	 * 计算总页数
	 * @param count 记录数
	 * @param countInPage 每页数量
	 * @return 总页数
	 */
	public static int getPageCount(int count, int countInPage){
		int mode = count % countInPage;
		int offset = (mode == 0)? 0:1;
		return (count / countInPage) + offset;
	}
	
}
