package com.shuangfeng.weixin.common.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.shuangfeng.weixin.common.dist.Const;

public class Util {

	
	@SuppressWarnings("unchecked")
	public static Class getParameterizedTypeClass(Object target, int genericParameterIndex) {
		ParameterizedType parameterizedType = getParameterizedType(target.getClass());
		return (Class) parameterizedType.getActualTypeArguments()[genericParameterIndex];
	}
	
	/**
	 * Get the first ParameterizedType according to the inherit class path.
	 * @param target
	 */
	@SuppressWarnings("unchecked")
	private static ParameterizedType getParameterizedType(Class clazz) {
		Type type = (Type)clazz.getGenericSuperclass();
		if(type != null && (type instanceof ParameterizedType))
			return (ParameterizedType)type;
		return getParameterizedType(clazz.getSuperclass());
	}
	
	public static BigDecimal getBigDecimalFromMapIgnoreKeyCase(Map<String, Object> map, String key) {
		String val = Util.getStringFromMapIgnoreKeyCase(map, key);
		if(!StringUtils.isEmpty(val))
			return new BigDecimal(val);
		return Const.ZERO_AMOUNT;
	}
	
	public static String getStringFromMapIgnoreKeyCase(Map<String, Object> map, String key) {
		Object data = null;
		if(map.containsKey(key))
			data = map.get(key);
		if(map.containsKey(key.toUpperCase()))
			data = map.get(key.toUpperCase());
		if(map.containsKey(key.toLowerCase()))
			data = map.get(key.toLowerCase());
		if(data != null)
			return data.toString();
		return "";
	}

	public static Object getInWhereCondition(Long[] itemIds) {
		String condition = "";
		for(Long l: itemIds){
			condition += String.valueOf(l) + ",";
		}
		return condition.substring(0, condition.length() -1);
	}
	
}
