package com.shuangfeng.weixin.common.config;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import com.shuangfeng.weixin.common.dist.Const;


public class SystemConfig {

	private static SystemConfig singleton;
	private Map<String, String> configData;
	
	private SystemConfig(){}
	
	public static synchronized SystemConfig getSingleton() {
		if(SystemConfig.singleton == null){
			SystemConfig.singleton = new SystemConfig();
			
			//get all the configuration data from "secrect.properties"
			SystemConfig.singleton.configData = new HashMap<String, String>();
			ResourceBundle bundle = PropertyResourceBundle.getBundle(Const.SECRECT_CONFIG_FILE);
			for (String key : bundle.keySet()) {
				SystemConfig.singleton.configData.put(key, bundle.getString(key));
			}
		}
		return singleton;
	}
	
	public String getConfigData(String key) {
		return this.configData.get(key);
	}
	
	public static String getMysqlHost(){
		return getDecodeData("mysql.host");
	}
	public static String getMysqlUser(){
		return getDecodeData("mysql.user");
	}
	public static String getMysqlPass(){
		return getDecodeData("mysql.pwd");
	}
	public static String getMysqlDB(){
		return getDecodeData("mysql.db");
	}
	private static String getDecodeData(String param){
		return getSingleton().getConfigData(param);
	}
	/**
	 * @return
	 */
	public static int getLoginMaxTryCount(){
		return Integer.valueOf(getSingleton().getConfigData("login.failure.maxtrycount"));
	}
	/**
	 * @return
	 */
	public static int getLoginUnLockHours(){
		return Integer.valueOf(getSingleton().getConfigData("login.failure.unlockhours"));
	}
	
	public static String getBaiduCloudChannel(){
		return getSingleton().getConfigData("BAIDU_COLUD_API_KEY");
	}
	
	public static String getBaiduCloudKey(){
		return getSingleton().getConfigData("BAIDU_COLUD_SECRET_KEY");
	}
}
