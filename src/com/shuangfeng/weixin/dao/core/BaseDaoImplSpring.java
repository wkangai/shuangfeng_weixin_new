package com.shuangfeng.weixin.dao.core;

import java.lang.reflect.ParameterizedType;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.shuangfeng.weixin.entity.core.BaseModel;

public abstract class BaseDaoImplSpring<T extends BaseModel> extends HibernateDaoSupport implements
		BaseDAO<T> {

	
	@SuppressWarnings("unchecked")
	protected Class<T> getTClass() {
		return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public T getItem(Long itemId) {
		return this.getHibernateTemplate().get(getTClass(), itemId);
	}

	public void save(T t) {
		this.getHibernateTemplate().save(t);
	}

	public void update(T t) {
		this.getHibernateTemplate().update(t);
	}

	public void delete(T t) {
		this.getHibernateTemplate().delete(t);
	}
	
}
