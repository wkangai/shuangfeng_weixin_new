package com.shuangfeng.weixin.dao.core;

import java.util.List;

import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.service.condition.core.Condition;



public interface ConditionDAO<T extends BaseModel> 
	extends BaseDAO<T>{

	<E> List<E> getItems(Condition condition);
	int getItemsCount(Condition condition);
	int getItemInWhichPageIndex(Condition condition, Long itemId);
	
}
