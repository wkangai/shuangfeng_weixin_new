package com.shuangfeng.weixin.dao.core;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;


import com.shuangfeng.weixin.common.dist.Const;
import com.shuangfeng.weixin.common.util.PageUtil;
import com.shuangfeng.weixin.common.util.Util;
import com.shuangfeng.weixin.dao.exception.InfrastructureException;
import com.shuangfeng.weixin.entity.core.BaseModel;
import com.shuangfeng.weixin.service.condition.core.BaseQueryCondition;
import com.shuangfeng.weixin.service.condition.core.Condition;

public abstract class ConditionDAOImpl<T extends BaseModel> 
	extends BaseDaoImplSpring<T>
	implements ConditionDAO<T>{

	private final static Log logger = LogFactory.getLog(ConditionDAOImpl.class);
	
	protected abstract Query getItemsQuery(Condition condition, Session session);
	protected abstract Query getItemCountQuery(Condition condition, Session session);
	
	protected String getSqlPartAlianWhere(Condition condition) {
		return Condition.A +" where " + condition.toCondition();
	}
	
	@SuppressWarnings("unchecked")
	public <E> List<E> getItems(Condition condition) {
		Session session = super.getSession();
		return this.getItems(condition, this.getItemsQuery(condition, session));
	}
	
	@SuppressWarnings("unchecked")
	public <E> List<E> getItems(Condition condition, Query query) {
		try{
			condition.postHandle(query);
			if(condition.getMaxResult() > 0){//If maxResult is set, then the page info is ignored.
				query.setMaxResults(condition.getMaxResult());
			}else if(condition.getPageIndex() >= 0){
				int beginIndex = condition.getCountInPage() * condition.getPageIndex();
				query
					.setFirstResult(beginIndex)
					.setMaxResults(condition.getCountInPage());
			}
			return (List<E>)query.list();
		}catch (HibernateException e) {
			logger.error("Error occured when calling getItems()", e);
			throw new InfrastructureException(e);
		}
	}

	public int getItemsCount(Condition condition) {
		Session session = super.getSession();
		return this.getItemsCount(condition, this.getItemCountQuery(condition, session));
	}
	
	public int getItemsCount(Condition condition, Query query) {
		try{
			condition.postHandle(query);
			query.setMaxResults(1);
			return Integer.valueOf(query.uniqueResult().toString());
		}catch (HibernateException e) {
			logger.error("Error occured when calling getItems()", e);
			throw new InfrastructureException(e);
		}
	}
	
	private String getSubSelect4GetItemInWhichPageIndex(Condition condition, String tableName, String itemIdColumn){
		String orderBy = condition.getOrderBy();
		if(StringUtils.isEmpty(orderBy)){
			orderBy = "";
		}
		BaseQueryCondition bq = (BaseQueryCondition)condition;
		return String.format(
				"select (@row:=@row+1) as row, " +
				"%5$s.%1$s " + 
				"from %2$s %5$s, (select  @row:=0) RT " +
				"where %3$s " +
				"%4$s ", 
				itemIdColumn,
				tableName,
				condition.toCondition(),
				orderBy,
				bq.getDefaultBaseTableAlian());
	}
	
	@SuppressWarnings("unchecked")
	public int getItemInWhichPageIndex(Condition condition, Long itemId) {
		Class baseModelClass = Util.getParameterizedTypeClass(this, 0);
		String tableName = baseModelClass.getSimpleName();
		String itemIdColumn = tableName.substring(0, 1).toLowerCase() 
											+ tableName.substring(1, tableName.length()) 
											+ "Id";//this is a rule of table name and table id column in this project
		BaseQueryCondition bq = (BaseQueryCondition)condition;
		String tableAlias = bq.getDefaultBaseTableAlian();
		String whereCondition = condition.toCondition();
		String orderBy = condition.getOrderBy();
		if(StringUtils.isEmpty(orderBy)){
			orderBy = "";
		}
		
		Session session = super.getSession();
		Query queryItemDataSql = session.createSQLQuery(
				"{CALL getPageIndexForItem(:tableName, :tableAlias, :whereCodition, :orderByConditon, :itemIdColumn, :itemId)}")
														.setString("tableName", tableName)
														.setString("tableAlias", tableAlias)
														.setString("whereCodition", whereCondition)
														.setString("orderByConditon", orderBy)
														.setString("itemIdColumn", itemIdColumn)
														.setLong("itemId", itemId)
												 		.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		Map<String, Object> itemData = (Map<String, Object>)queryItemDataSql.uniqueResult();
		if(itemData != null && itemData.containsKey("row")){
			BigDecimal row = Util.getBigDecimalFromMapIgnoreKeyCase(itemData, "row");
			if(!Const.ZERO_AMOUNT.equals(row)){
				int countInPage = condition.getCountInPage();
				return PageUtil.getPageCount(row.intValue(), countInPage);
			}
		}
		return 0;
	}
	
}
