package com.shuangfeng.weixin.dao.core;

import com.shuangfeng.weixin.entity.core.BaseModel;


/**
 * This DAO is a base interface for saving(insert or modify)
 * and deleting objects according to a database.
 * 
 *
 */
public interface BaseDAO<T extends BaseModel> {
	
	/**
	 * Get a item.
	 * @param itemId
	 * @return
	 */
	T getItem(Long itemId);
	
	/**
	 * Insert a new object or modify a existed object.
	 * 
	 * @param t
	 */
	void save(T t);
	
	void update(T t);
	
	/**
	 * Delete a object.
	 * @param t
	 */
	void delete(T t);
	
}
