package com.shuangfeng.weixin.dao;

import com.shuangfeng.weixin.dao.core.ConditionDAO;
import com.shuangfeng.weixin.entity.UserGroup;

public interface IUserGroupDao extends ConditionDAO<UserGroup> {
	UserGroup find(int groupId);
}
