package com.shuangfeng.weixin.dao.exception;

public class InfrastructureException extends RuntimeException {
	
	private static final long serialVersionUID = -7190019449967571968L;

	public InfrastructureException() {
	}

	public InfrastructureException(String message) {
		super(message);
	}

	public InfrastructureException(String message, Throwable cause) {
		super(message, cause);
	}

	public InfrastructureException(Throwable cause) {
		super(cause);
	}
	
}
