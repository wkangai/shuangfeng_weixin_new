package com.shuangfeng.weixin.dao.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import com.shuangfeng.weixin.dao.IUserDao;
import com.shuangfeng.weixin.dao.IUserGroupDao;
import com.shuangfeng.weixin.dao.core.ConditionDAOImpl;
import com.shuangfeng.weixin.entity.User;
import com.shuangfeng.weixin.entity.UserGroup;
import com.shuangfeng.weixin.service.condition.core.Condition;

public class UserDao extends ConditionDAOImpl<User> implements IUserDao {
	
	@Resource(name="userGroupDao")
	private IUserGroupDao userGroupDao;
	
	@Override
	protected Query getItemsQuery(Condition condition, Session session) {
		String orderBy = condition.getOrderBy();
		if(StringUtils.isEmpty(orderBy))
			orderBy = " order by u.createdDate desc ";
		return session.createQuery("from User u where " 
					+ condition.toCondition()
					+ orderBy);
	}

	@Override
	protected Query getItemCountQuery(Condition condition, Session session) {
		return session.createQuery(
				"select count(*) from User u where " + condition.toCondition());
	}

	@Override
	public User find(String userId) {
		Query query=getSession().createQuery("select from User as user where user.openId= :openId");
		query.setString("openId", userId);
		User user=(User)query.uniqueResult();
		return user;
	}

	@Override
	public void moveTo(Long id, Long groupId) {
		User user=getItem(id);		
		UserGroup originGroup=user.getGroup();
		if(originGroup==null || originGroup.getGroupId()!=groupId){
			UserGroup group=userGroupDao.getItem(groupId);
			user.setGroup(group);
			update(user);
		}		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<User> getSet(String hql) {
		Query query=getSession().createQuery(hql);
		List<User> users=query.list();
		Set<User> users2=new HashSet<User>(users);
		return users2;
	}
}
