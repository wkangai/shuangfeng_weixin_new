package com.shuangfeng.weixin.dao.impl;


import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import com.shuangfeng.weixin.dao.IDeveloperDao;
import com.shuangfeng.weixin.dao.core.ConditionDAOImpl;
import com.shuangfeng.weixin.entity.Developer;
import com.shuangfeng.weixin.service.condition.core.Condition;

public class DeveloperDao extends ConditionDAOImpl<Developer> implements
		IDeveloperDao {

	@Override
	protected Query getItemsQuery(Condition condition, Session session) {
		String orderBy = condition.getOrderBy();
		if(StringUtils.isEmpty(orderBy))
			orderBy = " order by d.createdDate desc ";
		return session.createQuery("from Developer d where " 
					+ condition.toCondition()
					+ orderBy);
	}

	@Override
	protected Query getItemCountQuery(Condition condition, Session session) {
		return session.createQuery(
		"select count(*) from Developer d where " + condition.toCondition());
	}

}
