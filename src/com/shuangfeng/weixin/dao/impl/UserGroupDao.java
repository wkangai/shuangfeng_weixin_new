package com.shuangfeng.weixin.dao.impl;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import com.shuangfeng.weixin.dao.IUserGroupDao;
import com.shuangfeng.weixin.dao.core.ConditionDAOImpl;
import com.shuangfeng.weixin.entity.UserGroup;
import com.shuangfeng.weixin.service.condition.core.Condition;

public class UserGroupDao extends ConditionDAOImpl<UserGroup> implements IUserGroupDao {
	@Override
	protected Query getItemsQuery(Condition condition, Session session) {
		String orderBy = condition.getOrderBy();
		if(StringUtils.isEmpty(orderBy))
			orderBy = " order by ug.createdDate desc ";
		return session.createQuery("from UserGroup ug where " 
					+ condition.toCondition()
					+ orderBy);
	}

	@Override
	protected Query getItemCountQuery(Condition condition, Session session) {
		return session.createQuery(
				"select count(*) from UserGroup ug where " + condition.toCondition());
	}

	@Override
	public UserGroup find(int groupId) {
		Session session=super.getSession();
		Query query=session.createQuery("select group from UserGroup as group where group.groupId= :groupId");
		query.setInteger("groupId", groupId);
		UserGroup group=(UserGroup)query.uniqueResult();
		return group;
	}
	 
}
