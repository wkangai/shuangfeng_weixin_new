package com.shuangfeng.weixin.dao.impl;


import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import com.shuangfeng.weixin.dao.IMenuDao;
import com.shuangfeng.weixin.dao.core.ConditionDAOImpl;
import com.shuangfeng.weixin.service.condition.core.Condition;
import com.shuangfeng.weixin.entity.Menu;

public class MenuDao extends ConditionDAOImpl<Menu> implements IMenuDao {

	@Override
	protected Query getItemsQuery(Condition condition, Session session) {
		String orderBy = condition.getOrderBy();
		if(StringUtils.isEmpty(orderBy))
			orderBy = " order by m.createdDate desc ";
		return session.createQuery("from Menu m where " 
					+ condition.toCondition()
					+ orderBy);
	}

	@Override
	protected Query getItemCountQuery(Condition condition, Session session) {
		return session.createQuery(
				"select count(*) from Menu m where " + condition.toCondition());
	}



	
}
