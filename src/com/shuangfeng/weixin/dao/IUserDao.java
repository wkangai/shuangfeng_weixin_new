package com.shuangfeng.weixin.dao;

import java.util.Set;

import com.shuangfeng.weixin.dao.core.ConditionDAO;
import com.shuangfeng.weixin.entity.User;

public interface IUserDao extends ConditionDAO<User>{
	User find(String userId);
	
	void moveTo(Long id,Long groupId);
	
	Set<User> getSet(String hql);
}
