package com.shuangfeng.weixin.dao;

import com.shuangfeng.weixin.dao.core.ConditionDAO;
import com.shuangfeng.weixin.entity.Developer;

public interface IDeveloperDao extends ConditionDAO<Developer> {

	
}
