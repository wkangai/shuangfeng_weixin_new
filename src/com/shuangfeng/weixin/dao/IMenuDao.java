package com.shuangfeng.weixin.dao;

import com.shuangfeng.weixin.dao.core.ConditionDAO;
import com.shuangfeng.weixin.entity.Menu;

public interface IMenuDao extends ConditionDAO<Menu> {

	
}
