<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/style/cms.css"/>
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/style/colorbox.css"/>
	<script src="<%=basePath%>/js/jquery-1.8.2.min.js" type="text/javascript"></script>
	<script src="<%=basePath%>/js/jquery.treeTable.js" type="text/javascript"></script>
	<script src="<%=basePath%>/js/jquery.colorbox-min.js" type="text/javascript"></script>
	<script src="<%=basePath%>/js/SimpleTemlate.js" type="text/javascript"></script>
	<script src="<%=basePath%>/js/My97/WdatePicker.js" type="text/javascript"></script>
	<script src="<%=basePath%>/js/jquery.cookie.js" type="text/javascript"></script>
	<script src="<%=basePath%>/js/tableStyle.js" type="text/javascript"></script>
