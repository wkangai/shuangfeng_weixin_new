<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@page import="com.shuangfeng.weixin.entity.Developer"%>
<%@page import="com.shuangfeng.weixin.entity.Developer.DeveloperState"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
Developer developer = (Developer)request.getAttribute("developer");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@ include file="../shared/header.jsp" %>
  <head>
  <title>标签分类管理</title>
	<script type="text/javascript">
	   $(document).ready(function(){
		    $("#focus .input_txt").each(function(){
			    var thisVal=$(this).val();
			    //判断文本框的值是否为空，有值的情况就隐藏提示语，没有值就显示
			    if(thisVal!=""){
			    $(this).siblings("span").hide();
			    }else{
			    $(this).siblings("span").show();
			    }
			    //聚焦型输入框验证
			    $(this).focus(function(){
			    $(this).siblings("span").hide();
			    }).blur(function(){
			    var val=$(this).val();
			    if(val!=""){
			    $(this).siblings("span").hide();
			    }else{
			    $(this).siblings("span").show();
			    }
			    });
		    });
	    $(".cms_search .input_txt").each(function(){
		    var thisVal=$(this).val();
		    //判断文本框的值是否为空，有值的情况就隐藏提示语，没有值就显示
		    if(thisVal!=""){
		    $(this).siblings("em").hide();
		    }else{
		    $(this).siblings("em").show();
		    }
		    $(this).keyup(function(){
		    var val=$(this).val();
		    $(this).siblings("em").hide();
		    }).blur(function(){
		    var val=$(this).val();
		    if(val!=""){
		    $(this).siblings("em").hide();
		    }else{
		    $(this).siblings("em").show();
		    }
		    });
		    });
		$(".anniu a").each(function () {
			$(this).append('<span class="fr"></span>');
			$(this).prepend('<span class="fl"></span>');
	   	});
	    });
    </script>
  </head>
  <body>
  <div class="anniu">
		<p class="fr">
		<a onclick="toModify()" ><span><b class="icon b1"></b>修改</span></a>
		</p>
  </div>
  <form:form method="post" modelAttribute="developer" name="showForm" id="showForm">
  	  <input type="hidden" id="developer_id" name="developer_id" value="${developer.id }"/>
	  <table class="add_zixun" width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	   <td class="l tr"><font>* </font>appId:</td>
	    <td class="r tl"><input type="text" class="textInput" disabled="disabled" id="appid" name="appid" value="${developer.appid}" /></td> 
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>appSecrect:</td>
	    <td class="r tl"><input type="text" class="textInput" disabled="disabled" id="appSecrect" name="appSecrect" value="${developer.appSecrect}" /></td> 
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>accessToken:</td>
	    <td class="r tl"><input type="text" class="textInput" disabled="disabled" id="accessToken" name="accessToken" value="${developer.accessToken}" /></td> 
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>token:</td>
	    <td class="r tl"><input type="text" class="textInput" disabled="disabled" id="token" name="token" value="${developer.token}" /></td> 
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>状态:</td>
	    <td class="r tl"><input type="text" class="textInput" disabled="disabled" id="state" name="state" value="<%=DeveloperState.get(developer.getState())%>" /></td> 
	  </tr>
	</table>
</form:form>
<script type="text/javascript">
$(document).ready(function(){
});
function toModify(){
	var checkedIds = $("#developer_id").val();
	var path = "<%=basePath%>developer/modify.jhtml?_modify_param_id="+checkedIds;
	$.colorbox({iframe:true,href:path, width:"50%", height:"50%"});
}
</script>
  </body>
</html>
