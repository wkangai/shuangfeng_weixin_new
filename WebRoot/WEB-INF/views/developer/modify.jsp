<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@page import="com.shuangfeng.weixin.entity.Developer"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
Developer developer = (Developer)request.getAttribute("developer");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@ include file="../shared/header.jsp" %>
  <head>
  <title>标签分类管理</title>
	<script type="text/javascript">
	   $(document).ready(function(){
		    $("#focus .input_txt").each(function(){
			    var thisVal=$(this).val();
			    //判断文本框的值是否为空，有值的情况就隐藏提示语，没有值就显示
			    if(thisVal!=""){
			    $(this).siblings("span").hide();
			    }else{
			    $(this).siblings("span").show();
			    }
			    //聚焦型输入框验证
			    $(this).focus(function(){
			    $(this).siblings("span").hide();
			    }).blur(function(){
			    var val=$(this).val();
			    if(val!=""){
			    $(this).siblings("span").hide();
			    }else{
			    $(this).siblings("span").show();
			    }
			    });
		    });
	    $(".cms_search .input_txt").each(function(){
		    var thisVal=$(this).val();
		    //判断文本框的值是否为空，有值的情况就隐藏提示语，没有值就显示
		    if(thisVal!=""){
		    $(this).siblings("em").hide();
		    }else{
		    $(this).siblings("em").show();
		    }
		    $(this).keyup(function(){
		    var val=$(this).val();
		    $(this).siblings("em").hide();
		    }).blur(function(){
		    var val=$(this).val();
		    if(val!=""){
		    $(this).siblings("em").hide();
		    }else{
		    $(this).siblings("em").show();
		    }
		    });
		    });
		$(".anniu a").each(function () {
			$(this).append('<span class="fr"></span>');
			$(this).prepend('<span class="fl"></span>');
	   	});
	    });
    </script>
  </head>
  <body>
  <form:form method="post" modelAttribute="developer" name="showForm" id="showForm">
  	  <input type="hidden" id="id" name="id" value="${developer.id }"/>
	  <table class="add_zixun" width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	    <td class="l tr"><font>* </font>appId:</td>
	    <td class="r tl"><input type="text" class="textInput" id="appid" name="appid" value="${developer.appid}" /></td> 
	  </tr>
	  <tr>
	    <td class="l tr"><font>* </font>appSecrect:</td>
	    <td class="r tl"><input type="text" class="textInput" id="appSecrect" name="appSecrect" value="${developer.appSecrect}" /></td> 
	  </tr>
	  <tr>
	    <td class="l tr"><font>* </font>accessToken:</td>
	    <td class="r tl"><input type="text" class="textInput" id="accessToken" name="accessToken" value="${developer.accessToken}" /></td> 
	  </tr>
	  <tr>
	    <td class="l tr"><font>* </font>token:</td>
	    <td class="r tl"><input type="text" class="textInput" id="token" name="token" value="${developer.token}" /></td> 
	  </tr>
	  <tr>
	    <td><input type="button" onclick="checkForm()" value="提交"></td>
	    <td><input type="reset" value="重置"/></td> 
	  </tr>
	</table>
</form:form>
<script type="text/javascript">
$(document).ready(function(){
});
function checkForm(){
	var a = $("#appid");
	var b = $("#appSecrect");
	var c = $("#accessToken");
	var d = $("#token");
	if(a.val()==''){
		alert("请输入appid!");
		return false;
	}
	if(b.val()==''){
		alert("请输入appSecrect!");
		return false;
	}
	if(c.val()==''){
		alert("请输入accessToken!");
		return false;
	}
	if(d.val()==''){
		alert("请输入token!");
		return false;
	}
	$.ajax({
		url:"<%=basePath%>developer/doModify.jhtml",
		type:"post",
		dataType:'json', 
        data: $("#showForm").serialize(),
		success:function(jsonModel){
			alert(jsonModel.message);
			if(jsonModel.status=="SUCCESS")
			{
				window.close();
				parent.location.reload();
			}
		}, 
		error:function(err){
			alert("发生异常!");
		}
	}); 
}
</script>
  </body>
</html>
