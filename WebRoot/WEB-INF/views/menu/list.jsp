<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@page import="com.shuangfeng.weixin.entity.Menu"%>
<%@page import="com.shuangfeng.weixin.common.util.JsonSerializer"%>
<%
List<Menu> menus = (List<Menu>)request.getAttribute("menus");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <%@ include file="../shared/header.jsp" %>
  <head>
  <title>菜单列表</title>
    <base href="<%=basePath%>">
    <script type="text/javascript">
    $(document).ready(function(){
		$(".anniu a").each(function () {
			$(this).append('<span class="fr"></span>');
			$(this).prepend('<span class="fl"></span>');
	   });
    });
    </script>
    
    <script src="js/jquery.treeTable.js" type="text/javascript"></script>
    
  </head>
  <body>
  <p class="w1000 cms_crumb tl">当前位置：自定义菜单管理</p>
	<div class="anniu">
		<p class="fr">
		<a onclick="toAdd()"><span><b class="icon b1"></b>添加菜单</span></a>
		<a onclick="toModify()"><span><b class="icon b4"></b>编辑</span></a>
		<a onclick="doDelete()"><span><b class="icon b5"></b>删除</span></a>
		<a onclick="doSync()"><span><b class="icon b3"></b>同步到服务器</span></a>
		</p>
	</div>
	<table class="zixun_table_tit font14" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
			    <th class="check" width="4%" height="39">
			      <input type="checkbox" name="checkAll1" class="allChcked"/>
			    </th>
			    <th width="8%">ID</th>
			    <th width="25%">类别</th>
			    <th width="25%">名称</th>
			    <th width="15%">key</th>
			    <th width="15%">url</th>
		   </tr>
	</table>
	
	<table id="treeBody" width="100%"  border="0" cellspacing="0" cellpadding="0">
			<%-- 
				<%
				for(Menu item:menus){
					%>
					<tr id="<%= item.getId() %>" pId="<%= item.getParent()!=null?item.getParent().getId():"" %>">
						<td class="check"  align="center" width="4%"><input type="checkbox" name="checkedId" id="checkedId" value="<%= item.getId()%>"/></td>
						<td width="8%" align="center"><%= item.getId() %></td>
						<td width="25%" align="center"><%= MenuResponseType.getObject(item.getType()) %></td>
						<td width="25%" align="center"><%= item.getName() %></td>
						<td width="15%" align="center"><%= item.getKeyName() %></td>
						<td width="15%" align="center"><%= item.getUrl() %></td>
					</tr>
					<%
				}%> 
			--%>
	 </table>
	 <script type="text/html" id="item_tmpl">
         <tr id="<#=id#>" pId="<#=parentId#>" >
            <td class="check" align="center" width="4%">
				<input type="checkbox" id="checkedId"  name="checkedId" value="<#=id#>"/>
			</td>
			<td width="8%" align="center"><#=id#></td>
            <td width="25%" align="center">
			<#=type_Name#>
			</td>
            <td width="25%" align="center"><#=name#></td>
			<td width="15%" align="center">
				<#if(typeof(keyName)=="undefined"){#>
				<#=''#>
				<#}else{#>
				<#=keyName#>
				<#}#>			
			</td>
 			<td width="15%" align="center">
				<#if(typeof(url)=="undefined"){#>
				<#=''#>
				<#}else{#>
				<#=url#>
				<#}#>
			</td>
          </tr>  
	</script>
	<script type="text/javascript">
		$(function(){
	        var menus= <%=JsonSerializer.SerializeObject(request.getAttribute("menus")) %>
	        if(menus.length>0){
	        	var arr = new Array();
		        function forData(cid,arr){
		        	for(var i=0;i<menus.length;i++){
		        		if(menus[i].parentId == cid){
		        			arr[arr.length] = tmpl("item_tmpl", menus[i]);
		        			forData(menus[i].id,arr);
		        		}
	 		        	
		        	}
		        }
	        	
		        forData(0,arr);
		 		var html=arr.join("");
		 		
		        $("#treeBody").html(html);
		            var option = {
		                theme:'vsStyle',
		                expandLevel:1
		            };
		        $('#treeBody').treeTable(option);
		        }
        });
        </script>
<script type="text/javascript">
$(document).ready(function(){
});

function toAdd(){
	var path = "<%=basePath%>menu/add.jhtml";
	$.colorbox({iframe:true,href:path, width:"60%", height:"50%"});
}

function toModify(){
	var checkedIds = getCheckedIds();
	if(checkedIds==''||checkedIds.indexOf(",")>0){
		alert("请选择一个项目");
		return false;
	}
	cleanCheckedBoxs();
	var path = "<%=basePath%>menu/modify.jhtml?_modify_param_id="+checkedIds;
	$.colorbox({iframe:true,href:path, width:"50%", height:"50%"});
}

function doDelete(){
	var checkedIds = getCheckedIds();
	if(checkedIds==''){
		alert("请选择项目");
		return false;
	}
	if(hasChildItems(checkedIds,"treeBody")){
		alert("有子类型,不可删除!");
		return false;
	}
	if(!confirm('确定删除吗?')){
		return false;
	}
	cleanCheckedBoxs();
	$.ajax({
		url:"<%=basePath%>menu/doDelete.jhtml",
		type:"post",
		dataType:'json', 
        data: {_general_param_ids:checkedIds},
		success:function(jsonModel){
			alert(jsonModel.message);
			if(jsonModel.status=="SUCCESS")
			{
				location.reload();
			}
		}, 
		error:function(err){
			alert("发生异常!");
		}
	}); 
}

function doSync(){
	$.ajax({
		url:"<%=basePath%>menu/doSync.jhtml",
		type:"post",
		dataType:'json', 
		success:function(jsonModel){
			if(jsonModel.errcode=="0")
			{
				location.reload();
			}else{
				alert("错误:"+jsonModel.errcode+",消息:"+jsonModel.errmsg);
			}
		}, 
		error:function(err){
			alert("发生异常!");
		}
	}); 
}

function hasChildItems(trIds,tableId){
	var trs = getCheckedTRs(trIds,tableId);
	for(i=0;i<trs.length;i++){
		if(trs[i].attr("hasChild")=="true"){
			return true;
		}
	}
	return false;
}

function getCheckedIds(){
	var checkedIds = '';
	$('input[name="checkedId"]:checked').each(function(){
		checkedIds+=$(this).val()+',';
	});
	checkedIds = checkedIds.substring(0, checkedIds.lastIndexOf(","));
	return checkedIds;
}
function getCheckedTRs(trIds,tableId){
	var idArray = trIds.split(",");
	var trs = new Array();
	$("#"+tableId).find("tr").each(function(){
		for(i=0;i<idArray.length;i++){
			if($(this).attr("id")==idArray[i]){
			trs.push($(this));
			}
		}
    });
    return trs;
}
function cleanCheckedBoxs(){
	$('input[name="checkedId"]:checked').each(function(){
		$(this).attr("checked",false);
	});
}
</script>
  </body>
</html>
