<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@page import="com.shuangfeng.weixin.entity.Menu"%>
<%@page import="com.shuangfeng.weixin.entity.dist.MenuResponseType"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%
List<Menu> menus = (List<Menu>)request.getAttribute("menus");
Menu menu = (Menu)request.getAttribute("menu");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@ include file="../shared/header.jsp" %>
  <head>
  <title>修改菜单</title>
	<script type="text/javascript">
	   $(document).ready(function(){
	    });
    </script>
  </head>
  <body>
  <form:form method="post" modelAttribute="menu" name="addForm" id="addForm">
  	<input type="hidden" id="id" name="id" value="${menu.id}"/>
	<table class="add_zixun" width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
	   	<td class="l tr"><font> </font>父菜单:</td>
	    <td class="r tl">
	    	<select class="textSelect" id="parent.id" name="parent.id" >
	    	<option value="" >  </option>
	  			<%
	  				for(int i=0;i<menus.size();i++)
	  				{
	  				 Menu item = menus.get(i);
	  					if(item.getId() == menu.getParentId())
	  					{
		  					%>
		  					<option value="<%=item.getId() %>" selected="selected"><%=item.getName() %></option>
		  					<%
	  				 	}else{
	  				 		%>
		  					<option value="<%=item.getId() %>"><%=item.getName() %></option>
		  					<%
	  				 	}
	  				}
	  			%>
	  		</select>
	  	</td> 
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>菜单类别:</td>
	   		<td class="r tl">		
		   		<select class="textSelect" id="type" name="type" >
		  			<%
		  				for(int i=0;i<MenuResponseType.values().length;i++)
		  				{
		  				 MenuResponseType item = MenuResponseType.values()[i];
		  					if(item.getValue() == menu.getType())
		  					{
			  					%>
			  					<option value="<%=item.getValue() %>" selected="selected"><%=item.getName() %></option>
			  					<%
		  				 	}else{
		  				 		%>
			  					<option value="<%=item.getValue() %>"><%=item.getName() %></option>
			  					<%
		  				 	}
		  				}
		  			%>
		  		</select>
	  		</td>
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>名称:</td>
	    <td class="r tl"><input type="text" class="textInput" id="name" name="name" value="<%=menu.getName() %>" /></td> 
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>key:</td>
	    <td class="r tl"><input type="text" class="textInput" id="keyName" name="keyName" value="<%=menu.getKeyName() %>" /></td> 
	  </tr>
	  <tr>
	   <td class="l tr"><font>* </font>url:</td>
	    <td class="r tl"><input type="text" class="textInput" id="url" name="url" value="<%=menu.getUrl() %>" /></td> 
	  </tr>
	  <tr>
	   <td><input type="button" onclick="checkForm()" value="提交"></td>
	    <td><input type="reset" value="重置"/></td> 
	  </tr>
  </table>
</form:form>
<script type="text/javascript">
$(document).ready(function(){
});
function checkForm(){
	var a = $("#type");
	var b = $("#name");
	var c = $("#keyName");
	var d = $("#url");
	if(a.val()==null || a.val()==''){
		alert("请选择菜单类别!");
		return false;
	}
	if(b.val()==''){
		alert("请输入菜单名称!");
		return false;
	}
	if(c.val()=='' && d.val()==''){
		alert("请输入key 或者 url!");
		return false;
	}
	$.ajax({
		url:"<%=basePath%>menu/doModify.jhtml",
		type:"post",
		dataType:'json', 
        data: $("#addForm").serialize(),
		success:function(jsonModel){
			alert(jsonModel.message);
			if(jsonModel.status=="SUCCESS")
			{
				window.close();
				parent.location.reload();
			}
		}, 
		error:function(err){
			alert("发生异常!");
		}
	}); 
}
</script>
  </body>
</html>
