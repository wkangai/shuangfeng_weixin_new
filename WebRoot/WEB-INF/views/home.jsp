<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ include file="shared/header.jsp" %>
<head>
<title>无标题文档</title>
 <script type="text/javascript">
        function CreateDiv(tabid, url, name)
         {
            ///如果当前tabid存在直接显示已经打开的tab
            if (document.getElementById("div_" + tabid) == null)
            {
                //创建iframe
                var box = document.createElement("iframe");
                box.id = "div_" + tabid;
                box.src = url;
                box.height = "100%";
                box.frameBorder = 0;
                box.width = "100%";
                document.getElementById("div_pannel").appendChild(box);
        
                //遍历并清除开始存在的tab当前效果并隐藏其显示的div
                var tablist = document.getElementById("div_tab").getElementsByTagName("li");
                var pannellist = document.getElementById("div_pannel").getElementsByTagName("iframe");
                if (tablist.length > 0)
                {
                    for (i = 0; i < tablist.length; i++)
                    {
                        tablist[i].className = "";
                        pannellist[i].style.display = "none";
                    }
                }
        
                //创建li菜单
                var tab = document.createElement("li");
                tab.className = "crent";
                tab.id = tabid;
				var litxt = '<span><a href="javascript:;" onclick=\'javascript:CreateDiv("' + tabid + '","' + url + '","' + name + '")\' title="' + name + '" class="menua">' + name + '</a><a onclick="RemoveDiv(\'' + tabid + '\')" class="win_close" title="关闭当前窗口"><a></span>';
                tab.innerHTML = litxt;
                document.getElementById("div_tab").appendChild(tab);
            }
            else
            {
                var tablist = document.getElementById("div_tab").getElementsByTagName("li");
                var pannellist = document.getElementById("div_pannel").getElementsByTagName("iframe");
                //alert(tablist.length);
                for (i = 0; i < tablist.length; i++)
                {
                    tablist[i].className = "";
                    pannellist[i].style.display = "none"
                }
                document.getElementById(tabid).className = "crent";
                document.getElementById("div_" + tabid).style.display = "block";
            }
        }
        function RemoveDiv(obj)
         {
            var ob = document.getElementById(obj);
            ob.parentNode.removeChild(ob);
            var obdiv = document.getElementById("div_" + obj);
            obdiv.parentNode.removeChild(obdiv);
            var tablist = document.getElementById("div_tab").getElementsByTagName("li");
            var pannellist = document.getElementById("div_pannel").getElementsByTagName("iframe");
            if (tablist.length > 0)
            {
                tablist[tablist.length - 1].className = "crent";
                pannellist[tablist.length - 1].style.display = "block";
            }
        }
    </script>
</head>

<body>

<div class="w1000 header">

<p class="fr wel"><span class="name"><%-- <%=userModel.getUid() %> --%></span>，你好！欢迎登录　　<span class="gfff"><a href="<%=basePath%>/logout.jhtml">退出</a></span></p>

<h2 class="logo"></h2>
<table class="menu" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="78">
    <ul class="font18 gfff">
    <li><a href="/">首页</a></li>
    </ul>
    </td>
  </tr>
</table>
</div>
<p class="fl wel" style="padding-top: 15px; padding-left: 2%"><span></span>&nbsp;&nbsp;</p>

<div class="w1000 tab_bq">
<ul class="clearfix" id="div_tab">

</ul>
</div>

<div class="fl tl left_menu">
	<h3 class="font16">系统设置</h3>
	<ul>
		<li>
		<a href="javascript:;" onclick='CreateDiv("category_list","developer/show.jhtml","开发者账号")'>开发者账号</a>
		</li>
	</ul>
	<h3 class="font16">菜单管理</h3>
	<ul>
		<li>
		<a href="javascript:;" onclick='CreateDiv("tagcategory_list","menu/list.jhtml","菜单管理")'>自定义菜单</a>
		</li>
	</ul>
	<h3 class="font16">用户管理</h3>
	<ul>
		<li>
		<a href="javascript:;" onclick='CreateDiv("tagcategory_list","group/list.jhtml","分组管理")'>分组管理</a>
		</li>
		<li>
		<a href="javascript:;" onclick='CreateDiv("tagcategory_list","user/list.jhtml","用户管理")'>用户管理</a>
		</li>
	</ul>
	<h3 class="font16">多媒体管理</h3>
	<ul>
		<li>
		<a href="javascript:;" onclick='CreateDiv("tagcategory_list","media/list.jhtml","多媒体管理")'>多媒体管理</a>
		</li>
	</ul>
	<h3 class="font16">消息管理</h3>
	<ul>
		<li>
		<a href="javascript:;" onclick='CreateDiv("tagcategory_list","message/receiveList.jhtml","消息接收")'>消息接收</a>
		</li>
		<li>
		<a href="javascript:;" onclick='CreateDiv("tagcategory_list","message/list.jhtml","响应消息")'>响应消息</a>
		</li>
		<li>
		<a href="javascript:;" onclick='CreateDiv("tagcategory_list","message/list.jhtml","群发消息")'>群发消息</a>
		</li>
	</ul>
</div>
<div class="right_box" id="div_pannel">

</div>
</body>
</html>
