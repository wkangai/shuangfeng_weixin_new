<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.shuangfeng.weixin.entity.User" %>
<%@page import="java.util.*" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <%@include file="../shared/base_header.jsp" %>
  </head>
  
  <body>
  
  	<button class="btn btn-primary btn-lg" data-toggle="modal" 
   	data-target="#myModal">
   	添加用户
	</button>
	
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   		<div class="modal-dialog">
      		<div class="modal-content">
      			<form id="user" class="form-horizontal" role="form" action="add.jhtml" method="post" accept-charset="utf-8">
   					<div class="form-group">
      					<label for="openId" class="col-sm-2 control-label">openId</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="openId" name="openId" placeholder="请输入Id">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="sex" class="col-sm-2 control-label">性别</label>
      					<div class="col-sm-1">
      						男：
      					</div>
      					<div class="col-sm-2">
         					<input type="radio" class="form-control" id="sex" name="sex" value="1">
      					</div>
      					<div class="col-sm-1">
      						女：
      					</div>
      					<div class="col-sm-2">
         					<input type="radio" class="form-control" id="sex" name="sex" value="0">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="nickName" class="col-sm-2 control-label">昵称</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="nickName" name="nickName" placeholder="请输入昵称">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="remark" class="col-sm-2 control-label">备注名称</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="remark" name="remark" placeholder="请输入备注">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="subscribeTime" class="col-sm-2 control-label">订阅时间</label>
      					<div class="col-sm-10">
         					<input type="date" class="form-control" id="subscribeTime" name="subscribeTime" placeholder="请输入订阅时间">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="country" class="col-sm-2 control-label">国家</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="country" name="country" placeholder="请输入国家">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="province" class="col-sm-2 control-label">省</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="province" name="province" placeholder="请输入省名">
      					</div>
   					</div> 
   					<div class="form-group">
      					<label for="city" class="col-sm-2 control-label">城市</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="city" name="city" placeholder="请输入城市名">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="language" class="col-sm-2 control-label">语言</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="language" name="language" placeholder="请输入语言名">
      					</div>
   					</div>  					 					
   					<div class="form-group">
      					<div class="col-sm-offset-2 col-sm-10">
         					<button type="submit" class="btn btn-default">提交</button>
      					</div>
   					</div>
				</form>
         	</div>
      	</div>
	</div>
    <table class="table table-striped">
    	<thead>
    		<tr>
    			<th>openId</th>
    			<th>性别</th>
    			<th>昵称</th>
    			<th>备注名称</th>
    			<th>订阅时间</th>
    			<th>国家</th>
    			<th>省份</th>
    		</tr>
    	</thead>
    	<tbody>
    		<c:forEach items="${users}" var="user">
    			<tr>
    				<th>${user.openId}</th>
    				<th>${user.sex}</th>
    				<th>${user.nickName}</th>
    				<th class="remark">${user.remark}</th>
    				<th>${user.subscribeTime}</th>
    				<th>${user.country}</th>
    				<th>${user.province}</th>
    				<th class="hide userId">${user.id}</th>    				    				
    			</tr>
    		</c:forEach>
    	</tbody>
    </table>
    <%@include file="../shared/base_js.jsp" %>
    <script>
    	$(document).ready(function(){
    	$("th.remark").dblclick(function(){
        	var thIns=$(this);
        	var id=thIns.siblings(".userId").text();
        	var text=$(this).text();
        	var inputIns=$("<input type='text'/>");        	
        	inputIns.width(thIns.width());
        	inputIns.val(thIns.html());
        	thIns.html("");
        	inputIns.appendTo(this).focus().select();
        	inputIns.click(function(){return false;});
	        inputIns.keyup(function(event){
	            var myEvent=event||window.event;
	            var key=myEvent.keyCode;
	            if(key==13){
	                changeText(thIns,$(this),text,id);           
	            }
	        });
	        inputIns.blur(function(){	             
				changeText(thIns,$(this),text,id);
	        });
	    });
	});
	function changeRemark(text,id){
		$.ajax({
		url:"changeRemark.jhtml",
		type:"post",
		dataType:'json', 
        data: {remark:text,id:id},
		success:function(jsonModel){			
			if(jsonModel.status=="SUCCESS")
			{
				alert(jsonModel.message);
			}
		}, 
		error:function(err){
			alert("发生异常!");
		}
	});
	}
	//检查修改后备注名称
	function changeText(thNode,inputNode,originText,id){
        var inputText=inputNode.val();
        thNode.html(inputText); 
        if(inputText!=originText){
        	changeRemark(inputText,id);
        } 
	}
    </script>
  </body>
</html>
