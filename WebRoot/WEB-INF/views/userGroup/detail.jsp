<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.shuangfeng.weixin.entity.User" %>
<%@page import="com.shuangfeng.weixin.entity.UserGroup" %>
<%@page import="java.util.*" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <%@include file="../shared/base_header.jsp" %>
  </head>
  
  <body>
  	<table class="table table-striped">
  		<thead>
    		<tr>
    			<th>groupId</th>
    			<th>名称</th>
    			<th>用户数量</th>
    		</tr>
    	</thead>
    	<tbody>
    			<tr data-toggle="cllapse" data-target="">
    				<th>${group.groupId}</th>
    				<th>${group.name}</th>
    				<th>${group.count}</th>  				    				
    			</tr>    			
    	</tbody>
  	</table>
  	<c:if test="${!empty group.users }">
  	<table class="table table-striped">  	  		
    	<thead>
    		<tr>
    			<th>openId</th>
    			<th>性别</th>
    			<th>昵称</th>
    			<th>备注名称</th>
    			<th>订阅时间</th>
    			<th>国家</th>
    			<th>省份</th>
    		</tr>
    	</thead>
    	<tbody>
    		<c:forEach items="${group.users }" var="user">
    			<tr>
    				<th>${user.openId}</th>
    				<th>${user.sex}</th>
    				<th>${user.nickName}</th>
    				<th class="remark">${user.remark}</th>
    				<th>${user.subscribeTime}</th>
    				<th>${user.country}</th>
    				<th>${user.province}</th>
    				<th class="hide userId">${user.id}</th>    				    				
    			</tr>
    		</c:forEach>
    	</tbody>
    </table>
    </c:if>
  <!--
  	<ul class="list-inline">
  		<li>分组Id</li>
  		<li>名称</li>
 		<li>用户数</li>
	</ul>
	<ul class="list-inline">
  		<li>${group.groupId }</li>
  		<li>${group.name }</li>
 		<li>${group.count }</li>
	</ul>	  
  	<div class="container">
  	  <div class="col-sm-4">
  	  	<p>分组Id</p>
  	  </div>
  	  <div class="col-sm-4">
  	  	<p>名称</p>
  	  </div>
  	  <div class="col-sm-4">
  	  	<p>用户数</p>
  	  </div>
  	  <div class="col-sm-4">
  	  	<p>${group.groupId }</p>
  	  </div>
  	  <div class="col-sm-4">
  	  	<p>${group.name }</p>
  	  </div>
  	  <div class="col-sm-4">
  	  	<p>${group.count }</p>
  	  </div>
  	</div>
  	-->
  	<%@include file="../shared/base_js.jsp" %>																		  	
  </body>
</html>
