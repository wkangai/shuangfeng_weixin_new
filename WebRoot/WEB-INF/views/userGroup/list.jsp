<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.shuangfeng.weixin.entity.User" %>
<%@page import="com.shuangfeng.weixin.entity.UserGroup" %>
<%@page import="java.util.*" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <%@include file="../shared/base_header.jsp" %>
  </head>
  
  <body> 
  	<button class="btn btn-primary btn-lg" data-toggle="modal" 
   	data-target="#myModal">
   	添加分组
	</button>
	
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   		<div class="modal-dialog">
      		<div class="modal-content">
      			<form id="group" class="form-horizontal" role="form" action="add.jhtml" method="post" accept-charset="utf-8">
   					<div class="form-group">
      					<label for="groupId" class="col-sm-2 control-label">groupId</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="groupId" name="groupId" placeholder="请输入Id">
      					</div>
   					</div>
   					<div class="form-group">
      					<label for="name" class="col-sm-2 control-label">名称</label>
      					<div class="col-sm-10">
         					<input type="text" class="form-control" id="name" name="name" placeholder="请输入名称">
      					</div>
   					</div>   										 					
   					<div class="form-group">
      					<div class="col-sm-offset-2 col-sm-10">
         					<button id="submit" type="button" class="btn" data-toggle="modal" data-target="#myModal">确定</button>
      					</div>
   					</div>
				</form>
         	</div>
      	</div>
	</div>
  	<table class="table table-striped">
    	<thead>
    		<tr>
    			<th>groupId</th>
    			<th>名称</th>
    			<th>用户数量</th>
    		</tr>
    	</thead>
    	<tbody>
    		<c:forEach items="${groups}" var="group">
    			<tr class="tr-click">
    				<th>${group.groupId}</th>
    				<th>${group.name}</th>
    				<th>${group.count}</th>
    				<th class="hide groupId">${group.id}</th>    				    				
    			</tr>
    		</c:forEach>
    	</tbody>
    </table>
  	<%@include file="../shared/base_js.jsp" %>
  	<script>
  		$(document).ready(function(){
    		$("#submit").click(function(){
       		 	$.ajax({
        			type: 'POST',
        			url: 'add.jhtml',
        			dataType: 'json',
        			data: $("#group").serialize(),
        			success:function(jsonModel){			
						if(jsonModel.status=="SUCCESS")
						{
							alert(jsonModel.message);
							window.close();
							parent.location.reload();
						}
					}, 
					error:function(err){
						alert("发生异常!");
					}        
        		});
    		});
		});
		$(".tr-click").click(function(){
    		var id=$(this).children(".groupId").text();
    		var location="detail.jhtml?id="+id;
    		window.location.href=location;
		});
  	</script>
  </body>
</html>
