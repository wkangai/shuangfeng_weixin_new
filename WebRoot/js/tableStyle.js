$(document).ready(
		
		function() {
			var tableConfig={overColor:"#FFF0AC",outColor:"#fff"};
			$(".tree_table tr").live("mouseover", function() {
				$(this).css("background-color", tableConfig.overColor);
			}).live(
					"mouseout",
					function() {
						if (typeof ($(this).find("input[type='checkbox']")
								.attr("checked")) == "undefined") {
							$(this).css("background-color", tableConfig.outColor);
						}
					}).live(
					"click",
					function() {
						if (typeof ($(this).find("input[type='checkbox']")
								.attr("checked")) != "undefined")  {
							$(this).css("background-color", tableConfig.overColor);
						}
					});
			
			$(".allChcked").click(function(){
				if($(this).prop("checked")){
					$(".tree_table tr").each(function(){
						$(this).css("background-color", tableConfig.overColor);
						$(this).find("input[type='checkbox']").eq(0).attr("checked","checked");
					})
				}
				else
				{
					$(".tree_table tr").each(function(){
						$(this).css("background-color", tableConfig.outColor);
						$(this).find("input[type='checkbox']").eq(0).removeAttr("checked");
					})
				}
			
			})
		})